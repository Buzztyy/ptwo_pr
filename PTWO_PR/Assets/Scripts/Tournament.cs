using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace PTWO_PR
{
    //This Script is not in use anymore. We kept it in the files to see what we need for our new Tournament System, as we implemented it
    public class Tournament : MonoBehaviour
{
    private DataManager dataManager;
    private MoneySystem moneySystem;

    public TextMeshProUGUI YouLost;   
    public TextMeshProUGUI YouWon;
    public TextMeshProUGUI YouAdvance1;
    public TextMeshProUGUI YouAdvance2;
    public TextMeshProUGUI TournamentWin;

    public Button tournamentButton;
    public Button tournamentButton2;
    public Button tournamentButton3;

    public Slider slider;
    public Gradient gradient;
    public Image fill;

    private void Awake()
    {
        dataManager = GameObject.FindWithTag("DataManager").GetComponent<DataManager>();
        moneySystem = GameObject.FindWithTag("MoneySystem").GetComponent<MoneySystem>();
    }

    private void Start()
    {
        slider.value = 0;
        slider.maxValue = 100;
        YouLost.gameObject.SetActive(false);
        YouWon.gameObject.SetActive(false);
        dataManager.Load();
    }

    IEnumerator LoadScene()
    {
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene("GameScene");
    }


    public void TournamentLogic()
    {
        tournamentButton.gameObject.SetActive(false);
        
        int randomValue = Random.Range(0, 100);
        
        int randomAdvance2Value = Random.Range(0, 100);
        int randomMoneyWinValue = Random.Range(50, 200);
        int randomMoneyLoseValue = Random.Range(20, 50);

        if (randomValue <= dataManager.data.winRate)
        {
            dataManager.data.money += randomMoneyWinValue;
            YouWon.gameObject.SetActive(true);
            YouAdvance1.gameObject.SetActive(true);
            slider.value += 33;
            SetProgress();
            dataManager.Save();
            tournamentButton2.gameObject.SetActive(true);
            moneySystem.MoneyUpdate();
        }

        else
        {
            dataManager.data.money += randomMoneyLoseValue;
            YouLost.gameObject.SetActive(true);
            dataManager.Save();
            moneySystem.MoneyUpdate();
            StartCoroutine(LoadScene());
        }
    }

    public void TournamentLogic2()
    {
        YouWon.gameObject.SetActive(false);
        YouAdvance1.gameObject.SetActive(false);
        tournamentButton2.gameObject.SetActive(false);
        int randomAdvance1Value = Random.Range(0, 100);
        int randomMoneyWinValue = Random.Range(120, 240);
        int randomMoneyLoseValue = Random.Range(40, 60);

        if (randomAdvance1Value <= dataManager.data.winRate)
        {
            YouAdvance1.gameObject.SetActive(false);
            dataManager.data.money += randomMoneyWinValue;
            YouAdvance2.gameObject.SetActive(true);
            YouWon.gameObject.SetActive(true);
            slider.value += 33;
            SetProgress();
            dataManager.Save();
            tournamentButton3.gameObject.SetActive(true);
            moneySystem.MoneyUpdate();
        }

        else
        {
            dataManager.data.money += randomMoneyLoseValue;
            YouLost.gameObject.SetActive(true);
            dataManager.Save();
            moneySystem.MoneyUpdate();
            StartCoroutine(LoadScene());
        }
    }

    public void TournamentLogic3()
    {
        YouWon.gameObject.SetActive(false);
        YouAdvance2.gameObject.SetActive(false);
        tournamentButton3.gameObject.SetActive(false);
        int randomAdvance2Value = Random.Range(0, 100);
        int randomMoneyWinValue = Random.Range(200, 420);
        int randomMoneyLoseValue = Random.Range(50, 70);

        if (randomAdvance2Value <= dataManager.data.winRate)
        {
            YouAdvance2.gameObject.SetActive(false);
            dataManager.data.money += randomMoneyWinValue;
            YouWon.gameObject.SetActive(true);
            TournamentWin.gameObject.SetActive(true);
            slider.value += 34;
            SetProgress();
            dataManager.Save();
            moneySystem.MoneyUpdate();
            StartCoroutine(LoadScene());
        }

        else
        {
            dataManager.data.money += randomMoneyLoseValue;
            YouLost.gameObject.SetActive(true);
            dataManager.Save();
            moneySystem.MoneyUpdate();
            StartCoroutine(LoadScene());
        }
    }

    public void SetProgress()
    {
        fill.color = gradient.Evaluate(slider.normalizedValue);
    }
}
}

