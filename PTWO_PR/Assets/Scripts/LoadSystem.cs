﻿using UnityEngine;
using System.Collections;
using TMPro;

namespace PTWO_PR
{
    public class LoadSystem : MonoBehaviour
{
    public DataManager dataManager;
    public CurrentPlayerSystem currentPlayerSystem;
    private ShopItemSystem shopItemSystem;
    private PlayerSetup playerSetup;
    private MoneySystem moneySystem;

    private void Awake()
    {
        dataManager = GameObject.FindWithTag("DataManager").GetComponent<DataManager>();
        currentPlayerSystem = GameObject.FindWithTag("CurrentPlayerSystem").GetComponent<CurrentPlayerSystem>();
        shopItemSystem = GameObject.FindWithTag("ShopItemSystem").GetComponent<ShopItemSystem>();
        playerSetup = GameObject.FindWithTag("PlayerSetup").GetComponent<PlayerSetup>();
        moneySystem = GameObject.FindWithTag("MoneySystem").GetComponent<MoneySystem>();
    }

        //used to load the player winrates into the play winrate array
        public void LoadPlayerWinRates()
    {
        playerSetup.playerWinRates[0] = dataManager.data.playerOneWinRate;
        playerSetup.playerWinRates[1] = dataManager.data.playerTwoWinRate;
        playerSetup.playerWinRates[2] = dataManager.data.playerThreeWinRate;
        playerSetup.playerWinRates[3] = dataManager.data.playerFourWinRate;
    }

        //loads the currentplayer upgrades into the shop item arrays
        public void LoadUpgradeFSM()
    {
        shopItemSystem.shopItems[0].upgradeFSM.Upgrades = currentPlayerSystem.currentPlayerSetupComputer;
        shopItemSystem.shopItems[1].upgradeFSM.Upgrades = currentPlayerSystem.currentPlayerSetupDesk;
        shopItemSystem.shopItems[2].upgradeFSM.Upgrades = currentPlayerSystem.currentPlayerSetupChair;
        shopItemSystem.shopItems[3].upgradeFSM.Upgrades = currentPlayerSystem.currentPlayerFitnessNutrition;
        shopItemSystem.shopItems[4].upgradeFSM.Upgrades = currentPlayerSystem.currentPlayerFitnessWorkout;
        shopItemSystem.shopItems[5].upgradeFSM.Upgrades = currentPlayerSystem.currentPlayerFitnessResting;
        shopItemSystem.shopItems[6].upgradeFSM.Upgrades = currentPlayerSystem.currentPlayerTrainingCoaching;
        shopItemSystem.shopItems[7].upgradeFSM.Upgrades = currentPlayerSystem.currentPlayerTrainingScrim;
        shopItemSystem.shopItems[8].upgradeFSM.Upgrades = currentPlayerSystem.currentPlayerTrainingBootcamp;
        Debug.Log("UgpradeFsm loaded");
    }

        //loads the saved upgrade status of all players into the corresponding arrays
        public void LoadPlayerUpgrades()
    {
        playerSetup.playerSetupComputer[0] = dataManager.data.playerOneSetupComputer;
        playerSetup.playerSetupComputer[1] = dataManager.data.playerTwoSetupComputer;
        playerSetup.playerSetupComputer[2] = dataManager.data.playerThreeSetupComputer;
        playerSetup.playerSetupComputer[3] = dataManager.data.playerFourSetupComputer;
        playerSetup.playerSetupDesk[0] = dataManager.data.playerOneSetupDesk;
        playerSetup.playerSetupDesk[1] = dataManager.data.playerTwoSetupDesk;
        playerSetup.playerSetupDesk[2] = dataManager.data.playerThreeSetupDesk;
        playerSetup.playerSetupDesk[3] = dataManager.data.playerFourSetupDesk;
        playerSetup.playerSetupChair[0] = dataManager.data.playerOneSetupChair;
        playerSetup.playerSetupChair[1] = dataManager.data.playerTwoSetupChair;
        playerSetup.playerSetupChair[2] = dataManager.data.playerThreeSetupChair;
        playerSetup.playerSetupChair[3] = dataManager.data.playerFourSetupChair;

        playerSetup.playerFitnessNutrition[0] = dataManager.data.playerOneFitnessNutrition;
        playerSetup.playerFitnessNutrition[1] = dataManager.data.playerTwoFitnessNutrition;
        playerSetup.playerFitnessNutrition[2] = dataManager.data.playerThreeFitnessNutrition;
        playerSetup.playerFitnessNutrition[3] = dataManager.data.playerFourFitnessNutrition;
        playerSetup.playerFitnessWorkout[0] = dataManager.data.playerOneFitnessWorkout;
        playerSetup.playerFitnessWorkout[1] = dataManager.data.playerTwoFitnessWorkout;
        playerSetup.playerFitnessWorkout[2] = dataManager.data.playerThreeFitnessWorkout;
        playerSetup.playerFitnessWorkout[3] = dataManager.data.playerFourFitnessWorkout;
        playerSetup.playerFitnessResting[0] = dataManager.data.playerOneFitnessResting;
        playerSetup.playerFitnessResting[1] = dataManager.data.playerTwoFitnessResting;
        playerSetup.playerFitnessResting[2] = dataManager.data.playerThreeFitnessResting;
        playerSetup.playerFitnessResting[3] = dataManager.data.playerFourFitnessResting;

        playerSetup.playerTrainingCoaching[0] = dataManager.data.playerOneTrainingCoaching;
        playerSetup.playerTrainingCoaching[1] = dataManager.data.playerTwoTrainingCoaching;
        playerSetup.playerTrainingCoaching[2] = dataManager.data.playerThreeTrainingCoaching;
        playerSetup.playerTrainingCoaching[3] = dataManager.data.playerFourTrainingCoaching;
        playerSetup.playerTrainingScrim[0] = dataManager.data.playerOneTrainingScrim;
        playerSetup.playerTrainingScrim[1] = dataManager.data.playerTwoTrainingScrim;
        playerSetup.playerTrainingScrim[2] = dataManager.data.playerThreeTrainingScrim;
        playerSetup.playerTrainingScrim[3] = dataManager.data.playerFourTrainingScrim;
        playerSetup.playerTrainingBootcamp[0] = dataManager.data.playerOneTrainingBootcamp;
        playerSetup.playerTrainingBootcamp[1] = dataManager.data.playerTwoTrainingBootcamp;
        playerSetup.playerTrainingBootcamp[2] = dataManager.data.playerThreeTrainingBootcamp;
        playerSetup.playerTrainingBootcamp[3] = dataManager.data.playerFourTrainingBootcamp;
    }


        //loads every shop price according to the number of upgrades
        public void LoadPrices()
    {
        if (shopItemSystem.shopItems[0].upgradeFSM.Upgrades <= 0)
        {
            moneySystem.computerPrice.text = shopItemSystem.shopItems[0].standardText;
        }

        else if (shopItemSystem.shopItems[0].upgradeFSM.Upgrades == 1)
        {
            moneySystem.computerPrice.text = shopItemSystem.shopItems[0].upgradeOneText;
        }

        else if (shopItemSystem.shopItems[0].upgradeFSM.Upgrades == 2)
        {
            moneySystem.computerPrice.text = shopItemSystem.shopItems[0].upgradeTwoText;
        }

        else if (shopItemSystem.shopItems[0].upgradeFSM.Upgrades == 3)
        {
            moneySystem.computerPrice.text = shopItemSystem.shopItems[0].upgradeThreeText;
        }

        if (shopItemSystem.shopItems[1].upgradeFSM.Upgrades <= 0)
        {
            moneySystem.deskPrice.text = shopItemSystem.shopItems[1].standardText;
        }

        else if (shopItemSystem.shopItems[1].upgradeFSM.Upgrades == 1)
        {
            moneySystem.deskPrice.text = shopItemSystem.shopItems[1].upgradeOneText;
        }

        else if (shopItemSystem.shopItems[1].upgradeFSM.Upgrades == 2)
        {
            moneySystem.deskPrice.text = shopItemSystem.shopItems[1].upgradeTwoText;
        }

        else if (shopItemSystem.shopItems[1].upgradeFSM.Upgrades == 3)
        {
            moneySystem.deskPrice.text = shopItemSystem.shopItems[1].upgradeThreeText;
        }

        if (shopItemSystem.shopItems[2].upgradeFSM.Upgrades <= 0)
        {
            moneySystem.chairPrice.text = shopItemSystem.shopItems[2].standardText;
        }

        else if (shopItemSystem.shopItems[2].upgradeFSM.Upgrades == 1)
        {
            moneySystem.chairPrice.text = shopItemSystem.shopItems[2].upgradeOneText;
        }

        else if (shopItemSystem.shopItems[2].upgradeFSM.Upgrades == 2)
        {
            moneySystem.chairPrice.text = shopItemSystem.shopItems[2].upgradeTwoText;
        }

        else if (shopItemSystem.shopItems[2].upgradeFSM.Upgrades == 3)
        {
            moneySystem.chairPrice.text = shopItemSystem.shopItems[2].upgradeThreeText;
        }

        if (shopItemSystem.shopItems[3].upgradeFSM.Upgrades <= 0)
        {
            moneySystem.nutritionPrice.text = shopItemSystem.shopItems[3].standardText;
        }

        else if (shopItemSystem.shopItems[3].upgradeFSM.Upgrades == 1)
        {
            moneySystem.nutritionPrice.text = shopItemSystem.shopItems[3].upgradeOneText;
        }

        else if (shopItemSystem.shopItems[3].upgradeFSM.Upgrades == 2)
        {
            moneySystem.nutritionPrice.text = shopItemSystem.shopItems[3].upgradeTwoText;
        }

        else if (shopItemSystem.shopItems[3].upgradeFSM.Upgrades == 3)
        {
            moneySystem.nutritionPrice.text = shopItemSystem.shopItems[3].upgradeThreeText;
        }

        if (shopItemSystem.shopItems[4].upgradeFSM.Upgrades <= 0)
        {
            moneySystem.workoutPrice.text = shopItemSystem.shopItems[4].standardText;
        }

        else if (shopItemSystem.shopItems[4].upgradeFSM.Upgrades == 1)
        {
            moneySystem.workoutPrice.text = shopItemSystem.shopItems[4].upgradeOneText;
        }

        else if (shopItemSystem.shopItems[4].upgradeFSM.Upgrades == 2)
        {
            moneySystem.workoutPrice.text = shopItemSystem.shopItems[4].upgradeTwoText;
        }

        else if (shopItemSystem.shopItems[4].upgradeFSM.Upgrades == 3)
        {
            moneySystem.workoutPrice.text = shopItemSystem.shopItems[4].upgradeThreeText;
        }

        if (shopItemSystem.shopItems[5].upgradeFSM.Upgrades <= 0)
        {
            moneySystem.restingPrice.text = shopItemSystem.shopItems[5].standardText;
        }

        else if (shopItemSystem.shopItems[5].upgradeFSM.Upgrades == 1)
        {
            moneySystem.restingPrice.text = shopItemSystem.shopItems[5].upgradeOneText;
        }

        else if (shopItemSystem.shopItems[5].upgradeFSM.Upgrades == 2)
        {
            moneySystem.restingPrice.text = shopItemSystem.shopItems[5].upgradeTwoText;
        }

        else if (shopItemSystem.shopItems[5].upgradeFSM.Upgrades == 3)
        {
            moneySystem.restingPrice.text = shopItemSystem.shopItems[5].upgradeThreeText;
        }

        if (shopItemSystem.shopItems[6].upgradeFSM.Upgrades <= 0)
        {
            moneySystem.coachingPrice.text = shopItemSystem.shopItems[6].standardText;
        }

        else if (shopItemSystem.shopItems[6].upgradeFSM.Upgrades == 1)
        {
            moneySystem.coachingPrice.text = shopItemSystem.shopItems[6].upgradeOneText;
        }

        else if (shopItemSystem.shopItems[6].upgradeFSM.Upgrades == 2)
        {
            moneySystem.coachingPrice.text = shopItemSystem.shopItems[6].upgradeTwoText;
        }

        else if (shopItemSystem.shopItems[6].upgradeFSM.Upgrades == 3)
        {
            moneySystem.coachingPrice.text = shopItemSystem.shopItems[6].upgradeThreeText;
        }

        if (shopItemSystem.shopItems[7].upgradeFSM.Upgrades <= 0)
        {
            moneySystem.scrimPrice.text = shopItemSystem.shopItems[7].standardText;
        }

        else if (shopItemSystem.shopItems[7].upgradeFSM.Upgrades == 1)
        {
            moneySystem.scrimPrice.text = shopItemSystem.shopItems[7].upgradeOneText;
        }

        else if (shopItemSystem.shopItems[7].upgradeFSM.Upgrades == 2)
        {
            moneySystem.scrimPrice.text = shopItemSystem.shopItems[7].upgradeTwoText;
        }

        else if (shopItemSystem.shopItems[7].upgradeFSM.Upgrades == 3)
        {
            moneySystem.scrimPrice.text = shopItemSystem.shopItems[7].upgradeThreeText;
        }

        if (shopItemSystem.shopItems[8].upgradeFSM.Upgrades <= 0)
        {
            moneySystem.bootcampPrice.text = shopItemSystem.shopItems[8].standardText;
        }

        else if (shopItemSystem.shopItems[8].upgradeFSM.Upgrades == 1)
        {
            moneySystem.bootcampPrice.text = shopItemSystem.shopItems[8].upgradeOneText;
        }

        else if (shopItemSystem.shopItems[8].upgradeFSM.Upgrades == 2)
        {
            moneySystem.bootcampPrice.text = shopItemSystem.shopItems[8].upgradeTwoText;
        }

        else if (shopItemSystem.shopItems[8].upgradeFSM.Upgrades == 3)
        {
            moneySystem.bootcampPrice.text = shopItemSystem.shopItems[8].upgradeThreeText;
        }
    }
}
}

