﻿using UnityEngine;
using System.Collections;

namespace PTWO_PR
{
    public class StartUpBehaviour : MonoBehaviour
    {
        // This Script Loads every information inside the Save/Load System on the games start-up
        private DataManager dataManager;
        private CurrentPlayerSystem currentPlayerSystem;
        private WinrateUI winrateUI;
        private MoneySystem moneySystem;
        private LoadSystem loadSystem;
        private PlayerSystem playerSystem;
        private UpdateSystem updateSystem;

        private void Awake()
        {
            dataManager = GameObject.FindWithTag("DataManager").GetComponent<DataManager>();
            currentPlayerSystem = GameObject.FindWithTag("CurrentPlayerSystem").GetComponent<CurrentPlayerSystem>();
            winrateUI = GameObject.FindWithTag("WinrateUI").GetComponent<WinrateUI>();
            moneySystem = GameObject.FindWithTag("MoneySystem").GetComponent<MoneySystem>();
            loadSystem = GameObject.FindWithTag("LoadSystem").GetComponent<LoadSystem>();
            playerSystem = GameObject.FindWithTag("PlayerSystem").GetComponent<PlayerSystem>();
            updateSystem = GameObject.FindWithTag("UpdateSystem").GetComponent<UpdateSystem>();
        }

        private void Start()
        {
            dataManager.Load();
            currentPlayerSystem.currentPlayerWinRate = dataManager.data.playerOneWinRate;
            moneySystem.MoneyUpdate();
            loadSystem.LoadPlayerWinRates();
            winrateUI.WinRateUpdate();
            playerSystem.ClickPlayerOne();
            updateSystem.UpdateUpgradeFSM();
            loadSystem.LoadPrices();
        }
    }
}

