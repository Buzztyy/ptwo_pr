﻿using UnityEngine;
using System.Collections;
using TMPro;

namespace PTWO_PR
{
    public class ShopItem : MonoBehaviour
{
    // This Script Manages all the items available in the shop to purchase, its prices and how often you can buy each item by storing the information inside a new State-Machine
    private DataManager dataManager;
    private WinrateUI winrateUI;
    private UpdateSystem updateSystem;
    private SaveSystem saveSystem;
    private CurrentPlayerSystem currentPlayerSystem;
    private MoneySystem moneySystem;

    public int upgradeState;

    [SerializeField]
    private int standardPrice;
    [SerializeField]
    private int upgradeOnePrice;
    [SerializeField]
    private int upgradeTwoPrice;

    [SerializeField]
    private TextMeshProUGUI itemPrice;
    [SerializeField]
    private TextMeshProUGUI money;

    public string standardText;
    public string upgradeOneText;
    public string upgradeTwoText;
    public string upgradeThreeText;


    public UpgradeFSM upgradeFSM;

    private void Awake()
    {
        dataManager = GameObject.FindWithTag("DataManager").GetComponent<DataManager>();
        winrateUI = GameObject.FindWithTag("WinrateUI").GetComponent<WinrateUI>();
        updateSystem = GameObject.FindWithTag("UpdateSystem").GetComponent<UpdateSystem>();
        saveSystem = GameObject.FindWithTag("SaveSystem").GetComponent<SaveSystem>();
        currentPlayerSystem = GameObject.FindWithTag("CurrentPlayerSystem").GetComponent<CurrentPlayerSystem>();
        moneySystem = GameObject.FindWithTag("MoneySystem").GetComponent<MoneySystem>();

        money = GameObject.FindWithTag("MoneyUI").GetComponent<TextMeshProUGUI>();
        Debug.Log("ASDASD");
        upgradeFSM = GetComponent<UpgradeFSM>();
    }

    public void UpgradeOnClick()
    {
        if (upgradeFSM.Upgrades <= 0)
        {
            if (dataManager.data.money >= standardPrice)
            {
                currentPlayerSystem.currentPlayerWinRate += standardPrice * 0.1f;
                upgradeFSM.Upgrades += 1;
                dataManager.data.money -= standardPrice;

                if (currentPlayerSystem.currentPlayerWinRate >= 100)
                {
                    currentPlayerSystem.currentPlayerWinRate = 100;
                }
            }

            else
                moneySystem.NoMoneyText();
        }

        else if (upgradeFSM.Upgrades == 1)
        {
            if (dataManager.data.money >= upgradeOnePrice)
            {
                currentPlayerSystem.currentPlayerWinRate += upgradeOnePrice * 0.1f;
                upgradeFSM.Upgrades += 1;
                dataManager.data.money -= upgradeOnePrice;

                if (currentPlayerSystem.currentPlayerWinRate >= 100)
                {
                    currentPlayerSystem.currentPlayerWinRate = 100;
                }
            }

            else
                moneySystem.NoMoneyText();
        }

        else if (upgradeFSM.Upgrades == 2)
        {
            if (dataManager.data.money >= upgradeTwoPrice)
            {
                currentPlayerSystem.currentPlayerWinRate += upgradeTwoPrice * 0.1f;
                upgradeFSM.Upgrades += 1;
                dataManager.data.money -= upgradeTwoPrice;

                if (currentPlayerSystem.currentPlayerWinRate >= 100)
                {
                    currentPlayerSystem.currentPlayerWinRate = 100;
                }
            }

            else
                moneySystem.NoMoneyText();
        }

        UpdateItemPrice();
        updateSystem.UpdateUpgradeFSM();
        saveSystem.SaveCurrentPlayer();
        winrateUI.WinRateUpdate();
        MoneyUpdate();
        dataManager.Save();
    }

    public void ResetUpgrades()
    {
        upgradeFSM.Upgrades = 0;
        itemPrice.text = standardText;
        MoneyUpdate();
    }

    public void MoneyUpdate()
    {
        money.text = "Money: " + dataManager.data.money.ToString() + " $";
    }

    public void UpdateItemPrice()
    {
        if (upgradeFSM.Upgrades == 0)
        {
            itemPrice.text = standardText;
        }

        else if (upgradeFSM.Upgrades == 1)
        {
            itemPrice.text = upgradeOneText;
        }

        else if (upgradeFSM.Upgrades == 2)
        {
            itemPrice.text = upgradeTwoText;
        }

        else if (upgradeFSM.Upgrades == 3)
        {
            itemPrice.text = upgradeThreeText;
        }
    }
}
}

