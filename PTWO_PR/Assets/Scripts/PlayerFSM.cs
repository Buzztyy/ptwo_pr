﻿using FSM;
using System;
using UnityEngine;

namespace PTWO_PR
{
    // This Script contains the Player-Finite-State-Machine. It manages the whole player switching process
    public enum PlayerFsmState
{
    // An Enum to store the PlayerFsmStates
    PlayerOne,
    PlayerTwo,
    PlayerThree,
    PlayerFour
}

public class PlayerFSM : MonoBehaviour
{
    [SerializeField]
    private PlayerFsmState[] states;
    [SerializeField]
    private PlayerFsmState startState;
    [SerializeField]
    private int playerSelected;
    public int PlayerSelected
    {
        get { return playerSelected; }
        set { playerSelected = value; }
    }
    [SerializeField]
    private PlayerFsmState currentState;

    private StateMachine fsm;


    private void Start()
    {
        // In Start we Initiate the FSM and add Transitions between different states
        fsm = new StateMachine();

        for (int i = 0; i < states.Length; i++)
        {

            fsm.AddState(states[i].ToString(), onEnter: OnEnterState, onLogic: OnLogicState);
        }

        fsm.AddTransition(PlayerFsmState.PlayerOne.ToString(), PlayerFsmState.PlayerTwo.ToString(), FromPlayerOneToPlayerTwo);

        fsm.AddTransition(PlayerFsmState.PlayerOne.ToString(), PlayerFsmState.PlayerThree.ToString(), FromPlayerOneToPlayerThree);

        fsm.AddTransition(PlayerFsmState.PlayerOne.ToString(), PlayerFsmState.PlayerFour.ToString(), FromPlayerOneToPlayerFour);

        fsm.AddTransition(PlayerFsmState.PlayerTwo.ToString(), PlayerFsmState.PlayerOne.ToString(), FromPlayerTwoToPlayerOne);

        fsm.AddTransition(PlayerFsmState.PlayerTwo.ToString(), PlayerFsmState.PlayerThree.ToString(), FromPlayerTwoToPlayerThree);

        fsm.AddTransition(PlayerFsmState.PlayerTwo.ToString(), PlayerFsmState.PlayerFour.ToString(), FromPlayerTwoToPlayerFour);

        fsm.AddTransition(PlayerFsmState.PlayerThree.ToString(), PlayerFsmState.PlayerOne.ToString(), FromPlayerThreeToPlayerOne);

        fsm.AddTransition(PlayerFsmState.PlayerThree.ToString(), PlayerFsmState.PlayerTwo.ToString(), FromPlayerThreeToPlayerTwo);

        fsm.AddTransition(PlayerFsmState.PlayerThree.ToString(), PlayerFsmState.PlayerFour.ToString(), FromPlayerThreeToPlayerFour);

        fsm.AddTransition(PlayerFsmState.PlayerFour.ToString(), PlayerFsmState.PlayerOne.ToString(), FromPlayerFourToPlayerOne);

        fsm.AddTransition(PlayerFsmState.PlayerFour.ToString(), PlayerFsmState.PlayerTwo.ToString(), FromPlayerFourToPlayerOne);

        fsm.AddTransition(PlayerFsmState.PlayerFour.ToString(), PlayerFsmState.PlayerThree.ToString(), FromPlayerFourToPlayerOne);

        fsm.SetStartState(startState.ToString());

        fsm.Init();
    }


    private void Update()
    {
        // Here we initiate the OnLogic method
        fsm.OnLogic();
    }

    private void OnEnterState(State<string> obj)
    {

        currentState = (PlayerFsmState)Enum.Parse(typeof(PlayerFsmState), obj.fsm.ActiveStateName);

        if (obj.fsm.ActiveStateName == PlayerFsmState.PlayerOne.ToString())
        {
            Debug.Log("Entering Player One State");
        }

        if (obj.fsm.ActiveStateName == PlayerFsmState.PlayerTwo.ToString())
        {
            Debug.Log("Entering Player Two State");
        }

        if (obj.fsm.ActiveStateName == PlayerFsmState.PlayerThree.ToString())
        {
            Debug.Log("Entering Player Three State");
        }

        if (obj.fsm.ActiveStateName == PlayerFsmState.PlayerFour.ToString())
        {
            Debug.Log("Entering Player Four State");
        }
    }


    private bool FromPlayerOneToPlayerTwo(Transition<string> arg)
    {
        if (playerSelected == 1)
        {
            Debug.Log("Transition successfull");
        }

        return playerSelected == 1;
    }

    private bool FromPlayerOneToPlayerThree(Transition<string> arg)
    {
        if (playerSelected == 2)
        {
            Debug.Log("Transition sucessfull");
        }

        return playerSelected == 2;
    }

    private bool FromPlayerOneToPlayerFour(Transition<string> arg)
    {
        if (playerSelected == 3)
        {
            Debug.Log("Transition sucessfull");
        }

        return playerSelected == 3;
    }

    private bool FromPlayerTwoToPlayerOne(Transition<string> arg)
    {
        if (playerSelected <= 0)
        {
            Debug.Log("Transition successfull");
        }

        return playerSelected <= 0;
    }

    private bool FromPlayerTwoToPlayerThree(Transition<string> arg)
    {
        if (playerSelected == 2)
        {
            Debug.Log("Transition sucessfull");
        }

        return playerSelected == 2;
    }

    private bool FromPlayerTwoToPlayerFour(Transition<string> arg)
    {
        if (playerSelected == 3)
        {
            Debug.Log("Transition sucessfull");
        }

        return playerSelected == 3;
    }

    private bool FromPlayerThreeToPlayerOne(Transition<string> arg)
    {
        if (playerSelected <= 0)
        {
            Debug.Log("Transition sucessfull");
        }

        return playerSelected <= 0;
    }

    private bool FromPlayerThreeToPlayerTwo(Transition<string> arg)
    {
        if (playerSelected <= 1)
        {
            Debug.Log("Transition sucessfull");
        }

        return playerSelected <= 1;
    }

    private bool FromPlayerThreeToPlayerFour(Transition<string> arg)
    {
        if (playerSelected == 3)
        {
            Debug.Log("Transition sucessfull");
        }

        return playerSelected == 3;
    }

    private bool FromPlayerFourToPlayerOne(Transition<string> arg)
    {
        if (playerSelected <= 0)
        {
            Debug.Log("Transition sucessfull");
        }

        return playerSelected <= 0;
    }

    private bool FromPlayerFourToPlayerTwo(Transition<string> arg)
    {
        if (playerSelected <= 1)
        {
            Debug.Log("Transition sucessfull");
        }

        return playerSelected <= 1;
    }

    private bool FromPlayerFourToPlayerThree(Transition<string> arg)
    {
        if (playerSelected <= 2)
        {
            Debug.Log("Transition sucessfull");
        }

        return playerSelected <= 2;
    }
    // This happening on Logic
    private void OnLogicState(State<string> obj)
    {
        if (obj.fsm.ActiveStateName == PlayerFsmState.PlayerOne.ToString())
        {
            OnPlayerOneStateUpdate();
        }

        if (obj.fsm.ActiveStateName == PlayerFsmState.PlayerTwo.ToString())
        {
            OnPlayerTwoStateUpdate();
        }

        if (obj.fsm.ActiveStateName == PlayerFsmState.PlayerThree.ToString())
        {
            OnPlayerThreeStateUpdate();
        }

        if (obj.fsm.ActiveStateName == PlayerFsmState.PlayerFour.ToString())
        {
            OnPlayerFourStateUpdate();
        }
    }
    // Debug Logs to see the selected  played
    private void OnPlayerOneStateUpdate()
    {
        Debug.Log("Player One Selected");
    }

    private void OnPlayerTwoStateUpdate()
    {
        Debug.Log("Player Two Selected");
    }

    private void OnPlayerThreeStateUpdate()
    {
        Debug.Log("Player Three Selected");
    }

    private void OnPlayerFourStateUpdate()
    {
        Debug.Log("Player Four Selected");
    }
}
}

