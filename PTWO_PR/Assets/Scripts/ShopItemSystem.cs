﻿using UnityEngine;
using System.Collections;

namespace PTWO_PR
{
    //creates an array for the shopitems
    public class ShopItemSystem : MonoBehaviour
    {
        public ShopItem[] shopItems;
        public PlayerSystem playerSystem;

        private void Awake()
        {
            playerSystem = GameObject.FindWithTag("PlayerSystem").GetComponent<PlayerSystem>();
        }
    }
}

