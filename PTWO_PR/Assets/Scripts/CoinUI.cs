﻿using UnityEngine;
using System.Collections;
using TMPro;

namespace PTWO_PR
{
    //This Script loads the money-data from our DataManager and displays it into the Scene
    public class CoinUI : MonoBehaviour
    {
        public TextMeshProUGUI coins;

        public DataManager dataManager;

        private void Start()
        {
            dataManager.Load();
            coins.text = dataManager.data.coins.ToString();
        }
    }
}

