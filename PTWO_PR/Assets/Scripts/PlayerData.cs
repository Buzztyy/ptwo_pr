﻿using UnityEngine;
using System.Collections;

namespace PTWO_PR
{
    // This Script stores the data of every player on the team. To change ingame-values, we change it here

    //used to save and load all the player data

    public class PlayerData
    {
        public int coins = 50;

        public int money = 500;

        public float winRate = 20;

        public float playerOneWinRate = 5;

        public float playerTwoWinRate = 5;

        public float playerThreeWinRate = 5;

        public float playerFourWinRate = 5;

        public float activePlayerWinRate;

        public int playerOneSetupComputer = 0;

        public int playerTwoSetupComputer = 0;

        public int playerThreeSetupComputer = 0;

        public int playerFourSetupComputer = 0;

        public int playerOneSetupDesk = 0;

        public int playerTwoSetupDesk = 0;

        public int playerThreeSetupDesk = 0;

        public int playerFourSetupDesk = 0;

        public int playerOneSetupChair = 0;

        public int playerTwoSetupChair = 0;

        public int playerThreeSetupChair = 0;

        public int playerFourSetupChair = 0;

        public int playerOneFitnessNutrition = 0;

        public int playerTwoFitnessNutrition = 0;

        public int playerThreeFitnessNutrition = 0;

        public int playerFourFitnessNutrition = 0;

        public int playerOneFitnessWorkout = 0;

        public int playerTwoFitnessWorkout = 0;

        public int playerThreeFitnessWorkout = 0;

        public int playerFourFitnessWorkout = 0;

        public int playerOneFitnessResting = 0;

        public int playerTwoFitnessResting = 0;

        public int playerThreeFitnessResting = 0;

        public int playerFourFitnessResting = 0;

        public int playerOneTrainingCoaching = 0;

        public int playerTwoTrainingCoaching = 0;

        public int playerThreeTrainingCoaching = 0;

        public int playerFourTrainingCoaching = 0;

        public int playerOneTrainingScrim = 0;

        public int playerTwoTrainingScrim = 0;

        public int playerThreeTrainingScrim = 0;

        public int playerFourTrainingScrim = 0;

        public int playerOneTrainingBootcamp = 0;

        public int playerTwoTrainingBootcamp = 0;

        public int playerThreeTrainingBootcamp = 0;

        public int playerFourTrainingBootcamp = 0;
    }
}

