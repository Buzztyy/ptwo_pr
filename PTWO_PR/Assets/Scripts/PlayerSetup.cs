﻿using UnityEngine;
using System.Collections;

namespace PTWO_PR
{
    public class PlayerSetup : MonoBehaviour
    {
        // This Script contains all playerData Arrays to store the data in
        public float[] playerWinRates;

        public int[] playerSetupComputer;
        public int[] playerSetupDesk;
        public int[] playerSetupChair;
        public int[] playerFitnessNutrition;
        public int[] playerFitnessWorkout;
        public int[] playerFitnessResting;
        public int[] playerTrainingCoaching;
        public int[] playerTrainingScrim;
        public int[] playerTrainingBootcamp;
    }
}

