﻿using UnityEngine;
using System.Collections;

namespace PTWO_PR
{
    public class ResetSystem : MonoBehaviour
    {
        private DataManager dataManager;
        private CurrentPlayerSystem currentPlayerSystem;
        private WinrateUI winrateUI;
        private UpgradeSystem upgradeSystem;
        private ShopItemSystem shopItemSystem;
        private MoneySystem moneySystem;

        private void Awake()
        {
            dataManager = GameObject.FindWithTag("DataManager").GetComponent<DataManager>();
            currentPlayerSystem = GameObject.FindWithTag("CurrentPlayerSystem").GetComponent<CurrentPlayerSystem>();
            winrateUI = GameObject.FindWithTag("WinrateUI").GetComponent<WinrateUI>();
            upgradeSystem = GameObject.FindWithTag("UpgradeSystem").GetComponent<UpgradeSystem>();
            shopItemSystem = GameObject.FindWithTag("ShopItemSystem").GetComponent<ShopItemSystem>();
            moneySystem = GameObject.FindWithTag("MoneySystem").GetComponent<MoneySystem>();
        }

        //resets upgrades done, not used anymore, replaced by "StartNewGame" method
        public void ResetUpgrades()
        {
            dataManager.data.playerOneWinRate = 5;
            dataManager.data.playerTwoWinRate = 5;
            dataManager.data.playerThreeWinRate = 5;
            dataManager.data.playerFourWinRate = 5;
            currentPlayerSystem.currentPlayerWinRate = 5;

            dataManager.data.playerOneSetupComputer = 0;
            dataManager.data.playerTwoSetupComputer = 0;
            dataManager.data.playerThreeSetupComputer = 0;
            dataManager.data.playerFourSetupComputer = 0;
            dataManager.data.playerOneSetupDesk = 0;
            dataManager.data.playerTwoSetupDesk = 0;
            dataManager.data.playerThreeSetupDesk = 0;
            dataManager.data.playerFourSetupDesk = 0;
            dataManager.data.playerOneSetupChair = 0;
            dataManager.data.playerTwoSetupChair = 0;
            dataManager.data.playerThreeSetupChair = 0;
            dataManager.data.playerFourSetupChair = 0;

            dataManager.data.playerOneFitnessNutrition = 0;
            dataManager.data.playerTwoFitnessNutrition = 0;
            dataManager.data.playerThreeFitnessNutrition = 0;
            dataManager.data.playerFourFitnessNutrition = 0;
            dataManager.data.playerOneFitnessWorkout = 0;
            dataManager.data.playerTwoFitnessWorkout = 0;
            dataManager.data.playerThreeFitnessWorkout = 0;
            dataManager.data.playerFourFitnessWorkout = 0;
            dataManager.data.playerOneFitnessResting = 0;
            dataManager.data.playerTwoFitnessResting = 0;
            dataManager.data.playerThreeFitnessResting = 0;
            dataManager.data.playerFourFitnessResting = 0;

            dataManager.data.playerOneTrainingCoaching = 0;
            dataManager.data.playerTwoTrainingCoaching = 0;
            dataManager.data.playerThreeTrainingCoaching = 0;
            dataManager.data.playerFourTrainingCoaching = 0;
            dataManager.data.playerOneTrainingScrim = 0;
            dataManager.data.playerTwoTrainingScrim = 0;
            dataManager.data.playerThreeTrainingScrim = 0;
            dataManager.data.playerFourTrainingScrim = 0;
            dataManager.data.playerOneTrainingBootcamp = 0;
            dataManager.data.playerTwoTrainingBootcamp = 0;
            dataManager.data.playerThreeTrainingBootcamp = 0;
            dataManager.data.playerFourTrainingBootcamp = 0;

            currentPlayerSystem.currentPlayerSetupComputer = 0;
            currentPlayerSystem.currentPlayerFitnessNutrition = 0;
            currentPlayerSystem.currentPlayerTrainingCoaching = 0;
            currentPlayerSystem.currentPlayerSetupDesk = 0;
            currentPlayerSystem.currentPlayerFitnessWorkout = 0;
            currentPlayerSystem.currentPlayerTrainingScrim = 0;
            currentPlayerSystem.currentPlayerSetupChair = 0;
            currentPlayerSystem.currentPlayerFitnessResting = 0;
            currentPlayerSystem.currentPlayerTrainingBootcamp = 0;

            upgradeSystem.computerUpgradeFSM.Upgrades = 0;
            upgradeSystem.deskUpgradeFSM.Upgrades = 0;
            upgradeSystem.chairUpgradeFSM.Upgrades = 0;
            upgradeSystem.nutritionUpgradeFSM.Upgrades = 0;
            upgradeSystem.workoutUpgradeFSM.Upgrades = 0;
            upgradeSystem.restingUpgradeFSM.Upgrades = 0;
            upgradeSystem.coachingUpgradeFSM.Upgrades = 0;
            upgradeSystem.scrimUpgradeFSM.Upgrades = 0;
            upgradeSystem.bootcampUpgradeFSM.Upgrades = 0;

            moneySystem.noMoney.text = "";

            dataManager.data.money = 500;

            shopItemSystem.shopItems[0].ResetUpgrades();
            shopItemSystem.shopItems[1].ResetUpgrades();
            shopItemSystem.shopItems[2].ResetUpgrades();
            shopItemSystem.shopItems[3].ResetUpgrades();
            shopItemSystem.shopItems[4].ResetUpgrades();
            shopItemSystem.shopItems[5].ResetUpgrades();
            shopItemSystem.shopItems[6].ResetUpgrades();
            shopItemSystem.shopItems[7].ResetUpgrades();
            shopItemSystem.shopItems[8].ResetUpgrades();

            dataManager.Save();
            winrateUI.WinRateUpdate();
            moneySystem.MoneyUpdate();
        }

        //begins a new game in the main menu
        public void StartNewGame()
        {
            dataManager.data.playerOneWinRate = 5;
            dataManager.data.playerTwoWinRate = 5;
            dataManager.data.playerThreeWinRate = 5;
            dataManager.data.playerFourWinRate = 5;

            dataManager.data.playerOneSetupComputer = 0;
            dataManager.data.playerTwoSetupComputer = 0;
            dataManager.data.playerThreeSetupComputer = 0;
            dataManager.data.playerFourSetupComputer = 0;
            dataManager.data.playerOneSetupDesk = 0;
            dataManager.data.playerTwoSetupDesk = 0;
            dataManager.data.playerThreeSetupDesk = 0;
            dataManager.data.playerFourSetupDesk = 0;
            dataManager.data.playerOneSetupChair = 0;
            dataManager.data.playerTwoSetupChair = 0;
            dataManager.data.playerThreeSetupChair = 0;
            dataManager.data.playerFourSetupChair = 0;

            dataManager.data.playerOneFitnessNutrition = 0;
            dataManager.data.playerTwoFitnessNutrition = 0;
            dataManager.data.playerThreeFitnessNutrition = 0;
            dataManager.data.playerFourFitnessNutrition = 0;
            dataManager.data.playerOneFitnessWorkout = 0;
            dataManager.data.playerTwoFitnessWorkout = 0;
            dataManager.data.playerThreeFitnessWorkout = 0;
            dataManager.data.playerFourFitnessWorkout = 0;
            dataManager.data.playerOneFitnessResting = 0;
            dataManager.data.playerTwoFitnessResting = 0;
            dataManager.data.playerThreeFitnessResting = 0;
            dataManager.data.playerFourFitnessResting = 0;

            dataManager.data.playerOneTrainingCoaching = 0;
            dataManager.data.playerTwoTrainingCoaching = 0;
            dataManager.data.playerThreeTrainingCoaching = 0;
            dataManager.data.playerFourTrainingCoaching = 0;
            dataManager.data.playerOneTrainingScrim = 0;
            dataManager.data.playerTwoTrainingScrim = 0;
            dataManager.data.playerThreeTrainingScrim = 0;
            dataManager.data.playerFourTrainingScrim = 0;
            dataManager.data.playerOneTrainingBootcamp = 0;
            dataManager.data.playerTwoTrainingBootcamp = 0;
            dataManager.data.playerThreeTrainingBootcamp = 0;
            dataManager.data.playerFourTrainingBootcamp = 0;

            dataManager.data.money = 500;

            dataManager.Save();
        }
    } 
}

