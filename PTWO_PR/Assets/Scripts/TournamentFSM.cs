using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using FSM;
using PGaS.IntermediateScripting._09_FiniteStateMachine;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;


namespace PTWO_PR
{
    // This Script Manages the TournamentFSM and contains the different state of the Tournament System. It works like every other FSM in our game
    public enum TournamentFSMState
    {
        IDLE,
        FIRST_GAME,
        SECOND_GAME,
        THIRD_GAME,
        TOURNAMENT_WIN
    }

    public class TournamentFSM : MonoBehaviour
    {
        [SerializeField] private TournamentFSMState[] states;

        [SerializeField] private TournamentFSMState startState = TournamentFSMState.IDLE;

        [SerializeField] private int tournamentRound;

        [SerializeField] private TournamentFSMState currentState;

        [SerializeField] private TournamentTextFSM tournamentTextFSM;

        [SerializeField] private TournamentConclusionFSM tournamentConclusionFSM;

        public TextMeshProUGUI textOne;
        public TextMeshProUGUI textTwo;
        public TextMeshProUGUI textThree;
        public TextMeshProUGUI textFour;
        public TextMeshProUGUI textFive;

        private StateMachine fsm;

        [SerializeField] private DataManager dataManager;
        [SerializeField] private MoneySystem moneySystem;

        public TextMeshProUGUI YouLost;
        public TextMeshProUGUI YouWon;
        public TextMeshProUGUI YouAdvance1;
        public TextMeshProUGUI YouAdvance2;
        public TextMeshProUGUI TournamentWin;

        public Button tournamentButton;
        public Button tournamentButton2;
        public Button tournamentButton3;

        private void Awake()
        {
            tournamentTextFSM = GameObject.FindWithTag("TournamentTextFSM").GetComponent<TournamentTextFSM>();
            tournamentConclusionFSM = GameObject.FindWithTag("TournamentConclusionFSM").GetComponent<TournamentConclusionFSM>();
        }

        private void Start()
        {
            fsm = new StateMachine();

            for (int i = 0; i < states.Length; i++)
            {
                fsm.AddState(states[i].ToString(), onEnter: OnEnterState, onLogic: OnLogicState);

                fsm.AddTransition(TournamentFSMState.IDLE.ToString(), TournamentFSMState.FIRST_GAME.ToString(), FromIdleToFirst);
                fsm.AddTransition(TournamentFSMState.FIRST_GAME.ToString(), TournamentFSMState.SECOND_GAME.ToString(), FromFirstToSecond);
                fsm.AddTransition(TournamentFSMState.SECOND_GAME.ToString(), TournamentFSMState.THIRD_GAME.ToString(), FromSecondToThird);

                fsm.Init();
            }
        }

        private void Update()
        {
            fsm.OnLogic();
        }

        private void OnEnterState(State<string> obj)
        {
            currentState = (TournamentFSMState) Enum.Parse(typeof(TournamentFSMState), obj.fsm.ActiveStateName);

            if (obj.fsm.ActiveStateName == TournamentFSMState.IDLE.ToString())
            {
                Debug.Log("Entering Idle State");
            }

            if (obj.fsm.ActiveStateName == TournamentFSMState.FIRST_GAME.ToString())
            {
                Debug.Log("Entering First_Game State");
            }

            if (obj.fsm.ActiveStateName == TournamentFSMState.SECOND_GAME.ToString())
            {
                Debug.Log("Entering Second_Game State");
            }

            if (obj.fsm.ActiveStateName == TournamentFSMState.THIRD_GAME.ToString())
            {
                Debug.Log("Entering Third_Game State");
            }

            if (obj.fsm.ActiveStateName == TournamentFSMState.TOURNAMENT_WIN.ToString())
            {
                Debug.Log("Entering Tournament_Win State");
            }
        }

        private void OnLogicState(State<string> obj)
        {
            if (obj.fsm.ActiveStateName == TournamentFSMState.IDLE.ToString())
            {
                OnIdleStateUpdate();
            }

            if (obj.fsm.ActiveStateName == TournamentFSMState.FIRST_GAME.ToString())
            {
                OnFirstGameStateUpdate();
            }

            if (obj.fsm.ActiveStateName == TournamentFSMState.SECOND_GAME.ToString())
            {
                OnSecondGameStateUpdate();
            }

            if (obj.fsm.ActiveStateName == TournamentFSMState.THIRD_GAME.ToString())
            {
                OnThirdGameStateUpdate();
            }

            if (obj.fsm.ActiveStateName == TournamentFSMState.TOURNAMENT_WIN.ToString())
            {
                OnTournamentWinStateUpdate();
            }
        }

        private void OnTournamentWinStateUpdate()
        {
            Debug.Log("Active State: TOURNAMENT_WIN");
        }
        
        private void OnThirdGameStateUpdate()
        {
            Debug.Log("Active State: THIRD_GAME");
        }

        private void OnSecondGameStateUpdate()
        {
            Debug.Log("Active State: SECOND_GAME");
        }

        private void OnFirstGameStateUpdate()
        {
            Debug.Log("Active State: FIRST_GAME");
        }

        private void OnIdleStateUpdate()
        {
            Debug.Log("Active State: IDLE");
        }

        private bool FromIdleToFirst(Transition<string> arg)
        {
            if (tournamentRound == 1)
            {
                tournamentButton.gameObject.SetActive(false);

                int randomValue = Random.Range(0, 100);

                if (randomValue <= dataManager.data.winRate)
                {
                    WinningTextOne();
                    tournamentConclusionFSM.TournamentStage = 1;
                }
                else
                {
                    LosingTextOne();
                    tournamentConclusionFSM.TournamentStage = 2;
                }
            }

            return tournamentRound == 1;
        }

        private bool FromFirstToSecond(Transition<string> arg)
        {
            if (tournamentRound == 2)
            {

                YouWon.gameObject.SetActive(false);
                YouAdvance1.gameObject.SetActive(false);
                tournamentButton2.gameObject.SetActive(false);
                int randomAdvance1Value = Random.Range(0, 100);

                if (randomAdvance1Value <= dataManager.data.winRate)
                {
                    WinningTextTwo();
                    tournamentConclusionFSM.TournamentStage = 3;
                }
                else
                {
                    LosingTextTwo();
                    tournamentConclusionFSM.TournamentStage = 4;
                }
            }

            return tournamentRound == 2;
        }

        private bool FromSecondToThird(Transition<string> arg)
        {
            if (tournamentRound == 3)
            {
                YouWon.gameObject.SetActive(false);
                YouAdvance2.gameObject.SetActive(false);
                tournamentButton3.gameObject.SetActive(false);
                
                int randomAdvance2Value = Random.Range(0, 100);

                if (randomAdvance2Value <= dataManager.data.winRate)
                {
                    WinningTextThree();
                    tournamentConclusionFSM.TournamentStage = 5;
                }
                else
                {
                    LosingTextThree();
                    tournamentConclusionFSM.TournamentStage = 6;
                }
            }

            return tournamentRound == 3;
        }

        public void TournamentLogic()
        {
            tournamentRound = 1;
            tournamentButton.gameObject.SetActive(false);
        }

        public void TournamentLogic2()
        {
            tournamentRound = 2;
            tournamentButton2.gameObject.SetActive(false);
        }

        public void TournamentLogic3()
        {
            tournamentRound = 3;
            tournamentButton3.gameObject.SetActive(false);
        }

        public void WinningTextOne()
        {
            textOne.text = "Let's see what Team A is going to have the upper hand today!";
            textTwo.text = "They starts off strong, holy shit, I didn't expect that!";
            textThree.text = "Team B really looks like they are struggling hard!";
            textFour.text = "They start losing everywhere on the map, ouch...";
            textFive.text = "Team A won with a huge lead, I've never seen such a stomp, Good Game!";


            tournamentTextFSM.TextNumber = 1;
        }

        public void WinningTextTwo()
        {
            textOne.text = "Team A is having a rough season so far, but they want the win this time!";
            textTwo.text = "Looking at Team B's Line-Up, I bet they are going for early pressure!";
            textThree.text = "Sike, I thought they had a chance for kills, but Team A is just better!";
            textFour.text = "Team A is overwhelmingly good, since when do they play that aggressive?";
            textFive.text = "What a loss for Team B, the better Team overall, but not as good as today's Team A!";

            tournamentTextFSM.TextNumber = 1;
        }

        public void WinningTextThree()
        {
            textOne.text = "Both teams have won every round today, lets see who's gonna win the millions!";
            textTwo.text = "Team A coming up with a good start-strategy, this could work out!";
            textThree.text = "Oh no, Team B knew their strategy, they are fighting back!";
            textFour.text = "What just happened?! Team A just overran Team B's territories!";
            textFive.text = "What a match, what a day, what a tournament, well played Team A, well f*king played!";

            tournamentTextFSM.TextNumber = 1;
        }

        private void LosingTextOne()
        {
            textOne.text = "Today both teams seem to be even in wins, let's see who will come out as the winner!";
            textTwo.text = "Team A is losing the first territory early, but no worries, they play for the late game!";
            textThree.text = "Oh no, Team B overtook half the mal! I don't know how Team A wants to keep up now.";
            textFour.text = "They are really trying to fight back, Team A is given their best!";
            textFive.text = "Unfortunately it wasn't enough, maybe next time Team A, you did well! GG to Team B!";

            tournamentTextFSM.TextNumber = 1;

        }

        public void LosingTextTwo()
        {
            textOne.text = "Up to a strong start Team A's Line-Up is looking weaker then last game...";
            textTwo.text = "Oh wow, I was wrong I guess, Team A is popping off!";
            textThree.text = "Since 15 minutes nothing happened from Team B's side, not a good winning condition!";
            textFour.text = "ACE! How did Team B kill Team A's whole Line-Up you ask? Pure skill and better fighting!";
            textFive.text = "With all territories in their hands Team B is rolling over Team A, GG!";

            tournamentTextFSM.TextNumber = 1;
        }

        public void LosingTextThree()
        {
            textOne.text = "Last game of the day! Let's make it a blast, good luck for everyone!";
            textTwo.text = "Both Teams are playing even, I have never seen a game that silent...";
            textThree.text = "The quiet before the storm guys! Team A with a huge fight!";
            textFour.text = "Team B is on a rampage now after the lost fight, They chase Team A!";
            textFive.text = "What a game, What! A! Game! I've never seen such a fighting, GG Team B, you deserve the win!";

            tournamentTextFSM.TextNumber = 1;
        }
    }
}
