﻿using UnityEngine;
using System.Collections;
using TMPro;

namespace PTWO_PR
{
    public class WinrateUI : MonoBehaviour
    {
        //This script manages the winrateUI System by loading, updating and saving the new winrate, if the player buys new items from the shop
        private DataManager dataManager;
        private LoadSystem loadSystem;
        private CurrentPlayerSystem currentPlayerSystem;

        public TextMeshProUGUI winRate;
        public TextMeshProUGUI playerOneWinRate;
        public TextMeshProUGUI playerTwoWinRate;
        public TextMeshProUGUI playerThreeWinRate;
        public TextMeshProUGUI playerFourWinRate;

        public WinrateBar winrateBar;
        public WinrateBar winrateBarPlayerOne;
        public WinrateBar winrateBarPlayerTwo;
        public WinrateBar winrateBarPlayerThree;
        public WinrateBar winrateBarPlayerFour;

        public float currentWinRate;

        private void Awake()
        {
            dataManager = GameObject.FindWithTag("DataManager").GetComponent<DataManager>();
            loadSystem = GameObject.FindWithTag("LoadSystem").GetComponent<LoadSystem>();
            currentPlayerSystem = GameObject.FindWithTag("CurrentPlayerSystem").GetComponent<CurrentPlayerSystem>();
        }

        public void WinRateUpdate()
        {
            loadSystem.LoadPlayerUpgrades();

            playerOneWinRate.text = dataManager.data.playerOneWinRate.ToString() + "%";
            playerTwoWinRate.text = dataManager.data.playerTwoWinRate.ToString() + "%";
            playerThreeWinRate.text = dataManager.data.playerThreeWinRate.ToString() + "%";
            playerFourWinRate.text = dataManager.data.playerFourWinRate.ToString() + "%";

            currentPlayerSystem.currentPlayerOneWinRate = dataManager.data.playerOneWinRate * 0.01f;
            currentPlayerSystem.currentPlayerTwoWinRate = dataManager.data.playerTwoWinRate * 0.01f;
            currentPlayerSystem.currentPlayerThreeWinRate = dataManager.data.playerThreeWinRate * 0.01f;
            currentPlayerSystem.currentPlayerFourWinRate = dataManager.data.playerFourWinRate * 0.01f;
            winrateBarPlayerOne.SetWinRate(currentPlayerSystem.currentPlayerOneWinRate);
            winrateBarPlayerTwo.SetWinRate(currentPlayerSystem.currentPlayerTwoWinRate);
            winrateBarPlayerThree.SetWinRate(currentPlayerSystem.currentPlayerThreeWinRate);
            winrateBarPlayerFour.SetWinRate(currentPlayerSystem.currentPlayerFourWinRate);

            dataManager.data.winRate = (dataManager.data.playerOneWinRate + dataManager.data.playerTwoWinRate +
                                        dataManager.data.playerThreeWinRate + dataManager.data.playerFourWinRate) / 4f;
            winRate.text = dataManager.data.winRate.ToString() + "%";
            currentWinRate = dataManager.data.winRate * 0.01f;
            winrateBar.SetWinRate(currentWinRate);
        }
    }
}
