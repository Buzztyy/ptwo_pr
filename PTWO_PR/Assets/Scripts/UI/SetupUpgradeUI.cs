using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace PTWO_PR
{
    public class SetupUpgradeUI : MonoBehaviour
    {
        [SerializeField] private string shopScene = "Shop";

        public void LoadShopScene()
        {
            SceneManager.LoadScene(shopScene);
        }
    }
}

