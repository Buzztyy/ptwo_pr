using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PTWO_PR
{
    public enum MenuState
    {
        OVERALLSHOPMENU,
        SETUPMENU,
        FITNESSMENU,
        TRAININGMENU,
        GAMEMENU
    }
    
    public class ShopUI : MonoBehaviour
    {
        [SerializeField] private string gameScene = "Game";
        [SerializeField] private string setupUpgradeScene = "SetupUpgrade";
        [SerializeField] private string bootcampScene = "Bootcamp";
    
        public GameObject overallShop;
        public GameObject setupMenu;
        public GameObject fitnessMenu;
        public GameObject trainingMenu;
        public GameObject gameMenu;
    
        private MenuState menuState;

        //method to switch UI elements based on selected menu
        public void MenuSwitch()
        {
            if (menuState == MenuState.OVERALLSHOPMENU)
            {
                overallShop.SetActive(true);
            }
            else
                overallShop.SetActive(false);
    
            if (menuState == MenuState.SETUPMENU)
            {
                setupMenu.SetActive(true);
            }
            else
                setupMenu.SetActive(false);
    
            if (menuState == MenuState.FITNESSMENU)
            {
                fitnessMenu.SetActive(true);
            }
            else
                fitnessMenu.SetActive(false);
    
            if (menuState == MenuState.TRAININGMENU)
            {
                trainingMenu.SetActive(true);
            }
            else
                trainingMenu.SetActive(false);
    
            if (menuState == MenuState.GAMEMENU)
            {
                gameMenu.SetActive(true);
            }
            else
                gameMenu.SetActive(false);
        }

        //methods used to change the UI corresponding to the menus
        #region Buttonmethods
        public void OverallShopMenu()
        {
            menuState = MenuState.OVERALLSHOPMENU;
            MenuSwitch();
        }
    
        public void OpenSetupMenu()
        {
            menuState = MenuState.SETUPMENU;
            MenuSwitch();
        }
    
        public void OpenFitnessMenu()
        {
            menuState = MenuState.FITNESSMENU;
            MenuSwitch();
        }
    
        public void OpenTrainingMenu()
        {
            menuState = MenuState.TRAININGMENU;
            MenuSwitch();
        }
    
        public void OpenGameMenu()
        {
            menuState = MenuState.GAMEMENU;
            MenuSwitch();
        }
        #endregion
    }
}

