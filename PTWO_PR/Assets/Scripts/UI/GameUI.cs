using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace PTWO_PR
{
    public class GameUI : MonoBehaviour
    {
        [SerializeField] private string menuScene = "MainMenu";
        [SerializeField] private string tournamentScene = "Tournament";

        public void LoadMenu()
        {
            SceneManager.LoadScene(menuScene);
        }

        public void LoadTournament()
        {
            SceneManager.LoadScene(tournamentScene);
        }
    }
}
