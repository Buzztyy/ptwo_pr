using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace PTWO_PR
{
    public class MenuUI : MonoBehaviour
    {
        [SerializeField] private string gameScene = "Game";
        ResetSystem resetSystem;

        private void Awake()
        {
            resetSystem = GameObject.FindWithTag("ResetSystem").GetComponent<ResetSystem>();
        }

        public void QuitGame()
        {
            Application.Quit();
        }
        
        

        public void StarNewGame()
        {
            resetSystem.StartNewGame();
            SceneManager.LoadScene(gameScene);
        }

        public void LoadGame()
        {
            SceneManager.LoadScene(gameScene);
        }
    }
}

