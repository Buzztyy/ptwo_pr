﻿using UnityEngine;
using System.Collections;

namespace PTWO_PR
{
    public class CurrentPlayerSystem : MonoBehaviour
    {
        //this scripts hold all the current player variables (winrate and upgrades)

        public float currentPlayerWinRate;

        public int currentPlayerSetupComputer;
        public int currentPlayerSetupDesk;
        public int currentPlayerSetupChair;
        public int currentPlayerFitnessNutrition;
        public int currentPlayerFitnessWorkout;
        public int currentPlayerFitnessResting;
        public int currentPlayerTrainingCoaching;
        public int currentPlayerTrainingScrim;
        public int currentPlayerTrainingBootcamp;

        public float currentPlayerOneWinRate;
        public float currentPlayerTwoWinRate;
        public float currentPlayerThreeWinRate;
        public float currentPlayerFourWinRate;
    }
}

