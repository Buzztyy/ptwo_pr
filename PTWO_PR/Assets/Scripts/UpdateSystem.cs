﻿using UnityEngine;
using System.Collections;

namespace PTWO_PR
{
    public class UpdateSystem : MonoBehaviour
    {
        private CurrentPlayerSystem currentPlayerSystem;
        private ShopItemSystem shopItemSystem;

        private void Awake()
        {
            currentPlayerSystem = GameObject.FindWithTag("CurrentPlayerSystem").GetComponent<CurrentPlayerSystem>();
            shopItemSystem = GameObject.FindWithTag("ShopItemSystem").GetComponent<ShopItemSystem>();
        }

        //updates the current player variables by using the shopsystem array
        public void UpdateUpgradeFSM()
        {
            currentPlayerSystem.currentPlayerSetupComputer = shopItemSystem.shopItems[0].upgradeFSM.Upgrades;
            currentPlayerSystem.currentPlayerSetupDesk = shopItemSystem.shopItems[1].upgradeFSM.Upgrades;
            currentPlayerSystem.currentPlayerSetupChair = shopItemSystem.shopItems[2].upgradeFSM.Upgrades;
            currentPlayerSystem.currentPlayerFitnessNutrition = shopItemSystem.shopItems[3].upgradeFSM.Upgrades;
            currentPlayerSystem.currentPlayerFitnessWorkout = shopItemSystem.shopItems[4].upgradeFSM.Upgrades;
            currentPlayerSystem.currentPlayerFitnessResting = shopItemSystem.shopItems[5].upgradeFSM.Upgrades;
            currentPlayerSystem.currentPlayerTrainingCoaching = shopItemSystem.shopItems[6].upgradeFSM.Upgrades;
            currentPlayerSystem.currentPlayerTrainingScrim = shopItemSystem.shopItems[7].upgradeFSM.Upgrades;
            currentPlayerSystem.currentPlayerTrainingBootcamp = shopItemSystem.shopItems[8].upgradeFSM.Upgrades;
        }
    }
}
