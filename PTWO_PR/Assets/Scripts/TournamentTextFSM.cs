﻿using UnityEngine;
using System.Collections;
using FSM;
using System;
using DG.Tweening;

namespace PTWO_PR
{
    public enum TournamentTextFsmState
    {
        NOTEXT,
        TEXTONE,
        TEXTTWO,
        TEXTTHREE,
        TEXTFOUR,
        TEXTFIVE
    }

    public class TournamentTextFSM : MonoBehaviour
    
    // This Script manages the displayed text during the tournament by putting it into a FSM
    {
        [SerializeField] private TournamentTextFsmState[] states;
        [SerializeField] private TournamentTextFsmState startState;
        [SerializeField] private int textNumber;
        [SerializeField] private TournamentFSM tournamentFSM;

        public int TextNumber
        {
            get { return textNumber; }
            set { textNumber = value; }
        }

        [SerializeField] private TournamentTextFsmState currentState;

        private StateMachine fsm;

        private void Awake()
        {
            tournamentFSM = GameObject.FindWithTag("TournamentFSM").GetComponent<TournamentFSM>();
        }


        private void Start()
        {

            fsm = new StateMachine();

            for (int i = 0; i < states.Length; i++)
            {

                fsm.AddState(states[i].ToString(), onEnter: OnEnterState, onLogic: OnLogicState);
            }

            fsm.AddTransition(TournamentTextFsmState.NOTEXT.ToString(), TournamentTextFsmState.TEXTONE.ToString(),
               FromNoTextToTextOne);

            fsm.AddTransition(TournamentTextFsmState.TEXTONE.ToString(), TournamentTextFsmState.TEXTTWO.ToString(),
                FromTextOneToTextTwo);

            fsm.AddTransition(TournamentTextFsmState.TEXTTWO.ToString(), TournamentTextFsmState.TEXTTHREE.ToString(),
                FromTextTwoToTextThree);

            fsm.AddTransition(TournamentTextFsmState.TEXTTHREE.ToString(), TournamentTextFsmState.TEXTFOUR.ToString(),
                FromTextThreeToTextFour);

            fsm.AddTransition(TournamentTextFsmState.TEXTFOUR.ToString(), TournamentTextFsmState.TEXTFIVE.ToString(),
                FromTextFourToTextFive);

            fsm.AddTransition(TournamentTextFsmState.TEXTFIVE.ToString(), TournamentTextFsmState.NOTEXT.ToString(),
                FromTextFiveToNoText);


            fsm.SetStartState(startState.ToString());

            fsm.Init();
        }


        private void Update()
        {
            fsm.OnLogic();
        }

        private void OnEnterState(State<string> obj)
        {

            currentState = (TournamentTextFsmState)Enum.Parse(typeof(TournamentTextFsmState), obj.fsm.ActiveStateName);

            if (obj.fsm.ActiveStateName == TournamentTextFsmState.NOTEXT.ToString())
            {
                Debug.Log("Entering NOTEXT State");
            }

            if (obj.fsm.ActiveStateName == TournamentTextFsmState.TEXTONE.ToString())
            {
                Debug.Log("Entering TEXTONE State");
            }

            if (obj.fsm.ActiveStateName == TournamentTextFsmState.TEXTTWO.ToString())
            {
                Debug.Log("Entering TEXTTWO State");
            }

            if (obj.fsm.ActiveStateName == TournamentTextFsmState.TEXTTHREE.ToString())
            {
                Debug.Log("Entering TEXTTHREE State");
            }

            if (obj.fsm.ActiveStateName == TournamentTextFsmState.TEXTFOUR.ToString())
            {
                Debug.Log("Entering TEXTFOUR State");
            }

            if (obj.fsm.ActiveStateName == TournamentTextFsmState.TEXTFIVE.ToString())
            {
                Debug.Log("Entering TEXTFIVE State");
            }
        }

        //starts coroutines based on the current state of text displayed while tournament is underway
        #region Transitionchanges
        private bool FromNoTextToTextOne(Transition<string> arg)
        {
            if (textNumber == 1)
            {
                StartCoroutine(EnableTextOne());
                Debug.Log("Transition successfull");
            }

            return textNumber == 1;
        }

        private bool FromTextOneToTextTwo(Transition<string> arg)
        {
            if (textNumber == 2)
            {
                StartCoroutine(EnableTextTwo());
                Debug.Log("Transition sucessfull");
            }

            return textNumber == 2;
        }

        private bool FromTextTwoToTextThree(Transition<string> arg)
        {
            if (textNumber == 3)
            {
                StartCoroutine(EnableTextThree());
                Debug.Log("Transition sucessfull");
            }

            return textNumber == 3;
        }

        private bool FromTextThreeToTextFour(Transition<string> arg)
        {
            if (textNumber == 4)
            {
                StartCoroutine(EnableTextFour());
                Debug.Log("Transition sucessfull");
            }

            return textNumber == 4;
        }

        private bool FromTextFourToTextFive(Transition<string> arg)
        {
            if (textNumber == 5)
            {
                StartCoroutine(EnableTextFive());
                Debug.Log("Transition sucessfull");
            }

            return textNumber == 5;
        }

        private bool FromTextFiveToNoText(Transition<string> arg)
        {
            if (textNumber <= 0)
            {
                StartCoroutine(DisableAllText());
                Debug.Log("Transition sucessfull");
            }

            return textNumber <= 0;
        }
        #endregion

        //Coroutines used to update the UI while a game in the tournament is underway
        #region Coroutines
        private IEnumerator EnableTextOne()
        {
            tournamentFSM.textOne.gameObject.SetActive(true);
            tournamentFSM.textOne.gameObject.transform.DOShakePosition(2, 10, 5, 3, true, true);
            yield return new WaitForSeconds(3.5f);
            textNumber = 2;
        }

        private IEnumerator EnableTextTwo()
        {
            tournamentFSM.textTwo.gameObject.SetActive(true);
            tournamentFSM.textTwo.gameObject.transform.DOShakePosition(2, 10, 5, 3, true, true);
            yield return new WaitForSeconds(3.5f);
            textNumber = 3;
        }

        private IEnumerator EnableTextThree()
        {
            tournamentFSM.textThree.gameObject.SetActive(true);
            tournamentFSM.textThree.gameObject.transform.DOShakePosition(2, 10, 5, 3, true, true);
            yield return new WaitForSeconds(3.5f);
            textNumber = 4;
        }

        private IEnumerator EnableTextFour()
        {
            tournamentFSM.textFour.gameObject.SetActive(true);
            tournamentFSM.textFour.gameObject.transform.DOShakePosition(2, 10, 5, 3, true, true);
            yield return new WaitForSeconds(3.5f);
            textNumber = 5;
        }

        private IEnumerator EnableTextFive()
        {
            tournamentFSM.textFive.gameObject.SetActive(true);
            tournamentFSM.textFive.gameObject.transform.DOShakePosition(2, 10, 5, 3, true, true);
            yield return new WaitForSeconds(4f);
            textNumber = 0;
        }

        private IEnumerator DisableAllText()
        {
            tournamentFSM.textOne.gameObject.SetActive(false);
            tournamentFSM.textTwo.gameObject.SetActive(false);
            tournamentFSM.textThree.gameObject.SetActive(false);
            tournamentFSM.textFour.gameObject.SetActive(false);
            tournamentFSM.textFive.gameObject.SetActive(false);
            yield return new WaitForSeconds(0.5f);
        }
        #endregion

        private void OnLogicState(State<string> obj)
        {
            if (obj.fsm.ActiveStateName == TournamentTextFsmState.NOTEXT.ToString())
            {
                OnTextOneStateUpdate();
            }

            if (obj.fsm.ActiveStateName == TournamentTextFsmState.TEXTONE.ToString())
            {
                OnTextOneStateUpdate();
            }

            if (obj.fsm.ActiveStateName == TournamentTextFsmState.TEXTTWO.ToString())
            {
                OnTextTwoStateUpdate();
            }

            if (obj.fsm.ActiveStateName == TournamentTextFsmState.TEXTTHREE.ToString())
            {
                OnTextThreeStateUpdate();
            }

            if (obj.fsm.ActiveStateName == TournamentTextFsmState.TEXTFOUR.ToString())
            {
                OnTextFourStateUpdate();
            }

            if (obj.fsm.ActiveStateName == TournamentTextFsmState.TEXTFIVE.ToString())
            {
                OnTextFiveStateUpdate();
            }
        }

        private void OnNoTextStateUpdate()
        {
            Debug.Log("In No Text");
        }

        private void OnTextOneStateUpdate()
        {
            Debug.Log("In Text One");
        }

        private void OnTextTwoStateUpdate()
        {
            Debug.Log("In Text Two");
        }

        private void OnTextThreeStateUpdate()
        {
            Debug.Log("In Text Three");
        }

        private void OnTextFourStateUpdate()
        {
            Debug.Log("In Text Four");
        }

        private void OnTextFiveStateUpdate()
        {
            Debug.Log("In Text Five");
        }
    }
}

