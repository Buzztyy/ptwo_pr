﻿using UnityEngine;
using System.Collections;
using FSM;
using System;

namespace PTWO_PR
{
     public enum UpgradeFsmState
    {
        Standard,
        UpgradeOne,
        UpgradeTwo,
        UpgradeThree
    }

    public class UpgradeFSM : MonoBehaviour
    {
        //This script manages the FSM for upgrading the players. It works like every other FSM in the game
        [SerializeField] private UpgradeFsmState[] states;
        [SerializeField] private UpgradeFsmState startState;
        [SerializeField] private int upgrades;

        public int Upgrades
        {
            get { return upgrades; }
            set { upgrades = value; }
        }

        [SerializeField] private UpgradeFsmState currentState;

        private StateMachine fsm;


        private void Start()
        {

            fsm = new StateMachine();

            for (int i = 0; i < states.Length; i++)
            {

                fsm.AddState(states[i].ToString(), onEnter: OnEnterState, onLogic: OnLogicState);
            }

            fsm.AddTransition(UpgradeFsmState.Standard.ToString(), UpgradeFsmState.UpgradeOne.ToString(),
                FromStandardToUpgradeOne);

            fsm.AddTransition(UpgradeFsmState.UpgradeOne.ToString(), UpgradeFsmState.Standard.ToString(),
                FromUpgradeOneToStandard);

            fsm.AddTransition(UpgradeFsmState.UpgradeOne.ToString(), UpgradeFsmState.UpgradeTwo.ToString(),
                FromUpgradeOneToUpgradeTwo);

            fsm.AddTransition(UpgradeFsmState.UpgradeTwo.ToString(), UpgradeFsmState.Standard.ToString(),
                FromUpgradeTwoToStandard);

            fsm.AddTransition(UpgradeFsmState.UpgradeTwo.ToString(), UpgradeFsmState.UpgradeThree.ToString(),
                FromUpgradeTwoToUpgradeThree);

            fsm.AddTransition(UpgradeFsmState.UpgradeThree.ToString(), UpgradeFsmState.Standard.ToString(),
                FromUpgradeThreeToStandard);

            fsm.AddTransition(UpgradeFsmState.Standard.ToString(), UpgradeFsmState.UpgradeTwo.ToString(),
                FromStandardToUpgradeTwo);

            fsm.AddTransition(UpgradeFsmState.Standard.ToString(), UpgradeFsmState.UpgradeThree.ToString(),
                FromStandardToUpgradeThree);


            fsm.SetStartState(startState.ToString());

            fsm.Init();
        }


        private void Update()
        {
            fsm.OnLogic();
        }

        private void OnEnterState(State<string> obj)
        {

            currentState = (UpgradeFsmState) Enum.Parse(typeof(UpgradeFsmState), obj.fsm.ActiveStateName);

            if (obj.fsm.ActiveStateName == UpgradeFsmState.Standard.ToString())
            {
                Debug.Log("Entering Standard State");
            }

            if (obj.fsm.ActiveStateName == UpgradeFsmState.UpgradeOne.ToString())
            {
                Debug.Log("Entering UpradeOne State");
            }

            if (obj.fsm.ActiveStateName == UpgradeFsmState.UpgradeTwo.ToString())
            {
                Debug.Log("Entering UpgradeTwo State");
            }

            if (obj.fsm.ActiveStateName == UpgradeFsmState.UpgradeThree.ToString())
            {
                Debug.Log("Entering UpgradeThree State");
            }
        }


        private bool FromStandardToUpgradeOne(Transition<string> arg)
        {
            if (upgrades == 1)
            {
                Debug.Log("Transition successfull");
            }

            return upgrades == 1;
        }

        private bool FromUpgradeOneToStandard(Transition<string> arg)
        {
            if (upgrades <= 0)
            {
                Debug.Log("Transition successfull");
            }

            return upgrades <= 0;
        }

        private bool FromUpgradeOneToUpgradeTwo(Transition<string> arg)
        {
            if (upgrades == 2)
            {
                Debug.Log("Transition sucessfull");
            }

            return upgrades == 2;
        }

        private bool FromUpgradeTwoToStandard(Transition<string> arg)
        {
            if (upgrades <= 0)
            {
                Debug.Log("Transition sucessfull");
            }

            return upgrades <= 0;
        }

        private bool FromUpgradeTwoToUpgradeThree(Transition<string> arg)
        {
            if (upgrades == 3)
            {
                Debug.Log("Transition sucessfull");
            }

            return upgrades == 3;
        }

        private bool FromUpgradeThreeToStandard(Transition<string> arg)
        {
            if (upgrades <= 0)
            {
                Debug.Log("Transition sucessfull");
            }

            return upgrades <= 0;
        }

        private bool FromStandardToUpgradeTwo(Transition<string> arg)
        {
            if (upgrades == 2)
            {
                Debug.Log("Transition sucessfull");
            }

            return upgrades == 2;
        }

        private bool FromStandardToUpgradeThree(Transition<string> arg)
        {
            if (upgrades == 3)
            {
                Debug.Log("Transition sucessfull");
            }

            return upgrades == 3;
        }

        private void OnLogicState(State<string> obj)
        {
            if (obj.fsm.ActiveStateName == UpgradeFsmState.Standard.ToString())
            {
                OnStandardStateUpdate();
            }

            if (obj.fsm.ActiveStateName == UpgradeFsmState.UpgradeOne.ToString())
            {
                OnUpgradeOneStateUpdate();
            }

            if (obj.fsm.ActiveStateName == UpgradeFsmState.UpgradeTwo.ToString())
            {
                OnUpgradeTwoStateUpdate();
            }

            if (obj.fsm.ActiveStateName == UpgradeFsmState.UpgradeThree.ToString())
            {
                OnUpgradeThreeStateUpdate();
            }
        }

        private void OnStandardStateUpdate()
        {
            Debug.Log("In Standard");
        }

        private void OnUpgradeOneStateUpdate()
        {
            Debug.Log("In UpgradeOne");
        }

        private void OnUpgradeTwoStateUpdate()
        {
            Debug.Log("In UpgradeTwo");
        }

        private void OnUpgradeThreeStateUpdate()
        {
            Debug.Log("In UpgradeThree");
        }
    }
}
