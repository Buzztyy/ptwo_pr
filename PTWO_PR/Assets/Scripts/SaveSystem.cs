﻿using UnityEngine;
using System.Collections;

namespace PTWO_PR
{
    public class SaveSystem : MonoBehaviour
{
    private CurrentPlayerSystem currentPlayerSystem;
    private DataManager dataManager;
    private ShopItemSystem shopItemSystem;
    private PlayerSetup playerSetup;

    private void Awake()
    {
        currentPlayerSystem = GameObject.FindWithTag("CurrentPlayerSystem").GetComponent<CurrentPlayerSystem>();
        dataManager = GameObject.FindWithTag("DataManager").GetComponent<DataManager>();
        shopItemSystem = GameObject.FindWithTag("ShopItemSystem").GetComponent<ShopItemSystem>();
        playerSetup = GameObject.FindWithTag("PlayerSetup").GetComponent<PlayerSetup>();
    }

        //saves the currentplayer data from the selected player to the json file
        public void SaveCurrentPlayer()
    {
        if (shopItemSystem.playerSystem.playerFSM.PlayerSelected == 0)
        {
            dataManager.data.playerOneWinRate = currentPlayerSystem.currentPlayerWinRate;
            dataManager.data.playerOneSetupComputer = currentPlayerSystem.currentPlayerSetupComputer;
            dataManager.data.playerOneSetupDesk = currentPlayerSystem.currentPlayerSetupDesk;
            dataManager.data.playerOneSetupChair = currentPlayerSystem.currentPlayerSetupChair;
            dataManager.data.playerOneFitnessNutrition = currentPlayerSystem.currentPlayerFitnessNutrition;
            dataManager.data.playerOneFitnessWorkout = currentPlayerSystem.currentPlayerFitnessWorkout;
            dataManager.data.playerOneFitnessResting = currentPlayerSystem.currentPlayerFitnessResting;
            dataManager.data.playerOneTrainingCoaching = currentPlayerSystem.currentPlayerTrainingCoaching;
            dataManager.data.playerOneTrainingScrim = currentPlayerSystem.currentPlayerTrainingScrim;
            dataManager.data.playerOneTrainingBootcamp = currentPlayerSystem.currentPlayerTrainingBootcamp;
        }

        if (shopItemSystem.playerSystem.playerFSM.PlayerSelected == 1)
        {
            dataManager.data.playerTwoWinRate = currentPlayerSystem.currentPlayerWinRate;
            dataManager.data.playerTwoSetupComputer = currentPlayerSystem.currentPlayerSetupComputer;
            dataManager.data.playerTwoSetupDesk = currentPlayerSystem.currentPlayerSetupDesk;
            dataManager.data.playerTwoSetupChair = currentPlayerSystem.currentPlayerSetupChair;
            dataManager.data.playerTwoFitnessNutrition = currentPlayerSystem.currentPlayerFitnessNutrition;
            dataManager.data.playerTwoFitnessWorkout = currentPlayerSystem.currentPlayerFitnessWorkout;
            dataManager.data.playerTwoFitnessResting = currentPlayerSystem.currentPlayerFitnessResting;
            dataManager.data.playerTwoTrainingCoaching = currentPlayerSystem.currentPlayerTrainingCoaching;
            dataManager.data.playerTwoTrainingScrim = currentPlayerSystem.currentPlayerTrainingScrim;
            dataManager.data.playerTwoTrainingBootcamp = currentPlayerSystem.currentPlayerTrainingBootcamp;
        }

        if (shopItemSystem.playerSystem.playerFSM.PlayerSelected == 2)
        {
            dataManager.data.playerThreeWinRate = currentPlayerSystem.currentPlayerWinRate;
            dataManager.data.playerThreeSetupComputer = currentPlayerSystem.currentPlayerSetupComputer;
            dataManager.data.playerThreeSetupDesk = currentPlayerSystem.currentPlayerSetupDesk;
            dataManager.data.playerThreeSetupChair = currentPlayerSystem.currentPlayerSetupChair;
            dataManager.data.playerThreeFitnessNutrition = currentPlayerSystem.currentPlayerFitnessNutrition;
            dataManager.data.playerThreeFitnessWorkout = currentPlayerSystem.currentPlayerFitnessWorkout;
            dataManager.data.playerThreeFitnessResting = currentPlayerSystem.currentPlayerFitnessResting;
            dataManager.data.playerThreeTrainingCoaching = currentPlayerSystem.currentPlayerTrainingCoaching;
            dataManager.data.playerThreeTrainingScrim = currentPlayerSystem.currentPlayerTrainingScrim;
            dataManager.data.playerThreeTrainingBootcamp = currentPlayerSystem.currentPlayerTrainingBootcamp;
        }

        if (shopItemSystem.playerSystem.playerFSM.PlayerSelected == 3)
        {
            dataManager.data.playerFourWinRate = currentPlayerSystem.currentPlayerWinRate;
            dataManager.data.playerFourSetupComputer = currentPlayerSystem.currentPlayerSetupComputer;
            dataManager.data.playerFourSetupDesk = currentPlayerSystem.currentPlayerSetupDesk;
            dataManager.data.playerFourSetupChair = currentPlayerSystem.currentPlayerSetupChair;
            dataManager.data.playerFourFitnessNutrition = currentPlayerSystem.currentPlayerFitnessNutrition;
            dataManager.data.playerFourFitnessWorkout = currentPlayerSystem.currentPlayerFitnessWorkout;
            dataManager.data.playerFourFitnessResting = currentPlayerSystem.currentPlayerFitnessResting;
            dataManager.data.playerFourTrainingCoaching = currentPlayerSystem.currentPlayerTrainingCoaching;
            dataManager.data.playerFourTrainingScrim = currentPlayerSystem.currentPlayerTrainingScrim;
            dataManager.data.playerFourTrainingBootcamp = currentPlayerSystem.currentPlayerTrainingBootcamp;
        }
    }

        //saves the player upgrades in the arrays to the json file
        public void SavePlayerUpgrades()
    {
        dataManager.data.playerOneSetupComputer = playerSetup.playerSetupComputer[0];
        dataManager.data.playerTwoSetupComputer = playerSetup.playerSetupComputer[1];
        dataManager.data.playerThreeSetupComputer = playerSetup.playerSetupComputer[2];
        dataManager.data.playerFourSetupComputer = playerSetup.playerSetupComputer[3];
        dataManager.data.playerOneSetupDesk = playerSetup.playerSetupDesk[0];
        dataManager.data.playerTwoSetupDesk = playerSetup.playerSetupDesk[1];
        dataManager.data.playerThreeSetupDesk = playerSetup.playerSetupDesk[2];
        dataManager.data.playerFourSetupDesk = playerSetup.playerSetupDesk[3];
        dataManager.data.playerOneSetupChair = playerSetup.playerSetupChair[0];
        dataManager.data.playerTwoSetupChair = playerSetup.playerSetupChair[1];
        dataManager.data.playerThreeSetupChair = playerSetup.playerSetupChair[2];
        dataManager.data.playerFourSetupChair = playerSetup.playerSetupChair[3];

        dataManager.data.playerOneFitnessNutrition = playerSetup.playerFitnessNutrition[0];
        dataManager.data.playerTwoFitnessNutrition = playerSetup.playerFitnessNutrition[1];
        dataManager.data.playerThreeFitnessNutrition = playerSetup.playerFitnessNutrition[2];
        dataManager.data.playerFourFitnessNutrition = playerSetup.playerFitnessNutrition[3];
        dataManager.data.playerOneFitnessWorkout = playerSetup.playerFitnessWorkout[0];
        dataManager.data.playerTwoFitnessWorkout = playerSetup.playerFitnessWorkout[1];
        dataManager.data.playerThreeFitnessWorkout = playerSetup.playerFitnessWorkout[2];
        dataManager.data.playerFourFitnessWorkout = playerSetup.playerFitnessWorkout[3];
        dataManager.data.playerOneFitnessResting = playerSetup.playerFitnessResting[0];
        dataManager.data.playerTwoFitnessResting = playerSetup.playerFitnessResting[1];
        dataManager.data.playerThreeFitnessResting = playerSetup.playerFitnessResting[2];
        dataManager.data.playerFourFitnessResting = playerSetup.playerFitnessResting[3];

        dataManager.data.playerOneTrainingCoaching = playerSetup.playerTrainingCoaching[0];
        dataManager.data.playerTwoTrainingCoaching = playerSetup.playerTrainingCoaching[1];
        dataManager.data.playerThreeTrainingCoaching = playerSetup.playerTrainingCoaching[2];
        dataManager.data.playerFourTrainingCoaching = playerSetup.playerTrainingCoaching[3];
        dataManager.data.playerOneTrainingScrim = playerSetup.playerTrainingScrim[0];
        dataManager.data.playerTwoTrainingScrim = playerSetup.playerTrainingScrim[1];
        dataManager.data.playerThreeTrainingScrim = playerSetup.playerTrainingScrim[2];
        dataManager.data.playerFourTrainingScrim = playerSetup.playerTrainingScrim[3];
        dataManager.data.playerOneTrainingBootcamp = playerSetup.playerTrainingBootcamp[0];
        dataManager.data.playerTwoTrainingBootcamp = playerSetup.playerTrainingBootcamp[1];
        dataManager.data.playerThreeTrainingBootcamp = playerSetup.playerTrainingBootcamp[2];
        dataManager.data.playerFourTrainingBootcamp = playerSetup.playerTrainingBootcamp[3];
    }
}
}

