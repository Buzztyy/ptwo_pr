﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace PTWO_PR
{
    public class TournamentConclusionSystem : MonoBehaviour
    {
        [SerializeField] private DataManager dataManager;
        [SerializeField] private MoneySystem moneySystem;
        [SerializeField] private AudioSource grandChampionSource;
        [SerializeField] private AudioClip grandChampionClip;

        public TextMeshProUGUI YouLost;
        public TextMeshProUGUI YouWon;
        public TextMeshProUGUI YouAdvance1;
        public TextMeshProUGUI YouAdvance2;
        public TextMeshProUGUI TournamentWin;

        public Button tournamentButton;
        public Button tournamentButton2;
        public Button tournamentButton3;

        public Slider slider;
        public Gradient gradient;
        public Image fill;

        private void Awake()
        {
            dataManager = GameObject.FindWithTag("DataManager").GetComponent<DataManager>();
            moneySystem = GameObject.FindWithTag("MoneySystem").GetComponent<MoneySystem>();
        }

        //updates UI and loads data
        private void Start()
        {
            slider.value = 0;
            slider.maxValue = 100;
            YouLost.gameObject.SetActive(false);
            YouWon.gameObject.SetActive(false);
            dataManager.Load();
        }

        //updates tournament progress bar
        public void SetProgress()
        {
            fill.color = gradient.Evaluate(slider.normalizedValue);
        }

        private IEnumerator LoadScene()
        {
            yield return new WaitForSeconds(3);
            SceneManager.LoadScene("GameScene");
        }

        //updates the UI, saves data, gives money and progresses the tournament state
        #region RoundLogic
        public IEnumerator FirstRoundWon()
        {
            yield return new WaitForSeconds(18f);
            int randomMoneyWinValue = Random.Range(280, 320);
            dataManager.data.money += randomMoneyWinValue;
            YouWon.gameObject.SetActive(true);
            YouAdvance1.gameObject.SetActive(true);
            slider.value += 33;
            SetProgress();
            dataManager.Save();
            tournamentButton2.gameObject.SetActive(true);
            moneySystem.MoneyUpdate();
        }

        public IEnumerator SecondRoundWon()
        {
            yield return new WaitForSeconds(18f);
            Debug.Log("GOT THE MONEY");
            int randomMoneyWinValue = Random.Range(350, 400);
            dataManager.data.money += randomMoneyWinValue;
            YouAdvance1.gameObject.SetActive(false);
            dataManager.data.money += randomMoneyWinValue;
            YouAdvance2.gameObject.SetActive(true);
            YouWon.gameObject.SetActive(true);
            Debug.Log("YOU WON TEXT LOL");
            slider.value += 33;
            SetProgress();
            dataManager.Save();
            tournamentButton3.gameObject.SetActive(true);
            moneySystem.MoneyUpdate();
            Debug.Log("FINISHED SEOND ROUND ROFLL");
        }

        public IEnumerator ThirdRoundWon()
        {
            yield return new WaitForSeconds(18f);
            int randomMoneyWinValue = Random.Range(600, 700);
            dataManager.data.money += randomMoneyWinValue;
            YouAdvance2.gameObject.SetActive(false);
            dataManager.data.money += randomMoneyWinValue;
            YouWon.gameObject.SetActive(true);
            TournamentWin.gameObject.SetActive(true);
            slider.value += 34;
            SetProgress();
            dataManager.Save();
            moneySystem.MoneyUpdate();
            grandChampionSource.PlayOneShot(grandChampionClip);
            StartCoroutine(LoadScene());
        }

        public IEnumerator FirstRoundLost()
        {
            yield return new WaitForSeconds(18f);
            int randomMoneyLoseValue = Random.Range(100, 120);
            dataManager.data.money += randomMoneyLoseValue;
            YouLost.gameObject.SetActive(true);
            dataManager.Save();
            moneySystem.MoneyUpdate();
            yield return new WaitForSeconds(2.5f);
            SceneManager.LoadScene("GameScene");
        }

        public IEnumerator SecondRoundLost()
        {
            yield return new WaitForSeconds(18f);
            int randomMoneyLoseValue = Random.Range(140, 180);
            dataManager.data.money += randomMoneyLoseValue;
            YouLost.gameObject.SetActive(true);
            dataManager.Save();
            moneySystem.MoneyUpdate();
            yield return new WaitForSeconds(2.5f);
            SceneManager.LoadScene("GameScene");
        }

        public IEnumerator ThirdRoundLost()
        {
            yield return new WaitForSeconds(18f);
            int randomMoneyLoseValue = Random.Range(200, 240);
            dataManager.data.money += randomMoneyLoseValue;
            YouLost.gameObject.SetActive(true);
            dataManager.Save();
            moneySystem.MoneyUpdate();
            yield return new WaitForSeconds(2.5f);
            SceneManager.LoadScene("GameScene");
        }
        #endregion
    }
}
