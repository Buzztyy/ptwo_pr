﻿using UnityEngine;
using System.Collections;

namespace PTWO_PR
{
    public class UpgradeSystem : MonoBehaviour
    {
        private ShopItemSystem shopItemSystem;
        private CurrentPlayerSystem currentPlayerSystem;

        public UpgradeFSM computerUpgradeFSM;
        public UpgradeFSM deskUpgradeFSM;
        public UpgradeFSM chairUpgradeFSM;
        public UpgradeFSM nutritionUpgradeFSM;
        public UpgradeFSM workoutUpgradeFSM;
        public UpgradeFSM restingUpgradeFSM;
        public UpgradeFSM coachingUpgradeFSM;
        public UpgradeFSM scrimUpgradeFSM;
        public UpgradeFSM bootcampUpgradeFSM;

        private void Awake()
        {
            shopItemSystem = GameObject.FindWithTag("ShopItemSystem").GetComponent<ShopItemSystem>();
            currentPlayerSystem = GameObject.FindWithTag("CurrentPlayerSystem").GetComponent<CurrentPlayerSystem>();
        }

        //updates the shopsystem arrays by using the currentplayer variables
        public void UpdateUpgradeFSM()
        {
            shopItemSystem.shopItems[0].upgradeFSM.Upgrades = currentPlayerSystem.currentPlayerSetupComputer;
            shopItemSystem.shopItems[1].upgradeFSM.Upgrades = currentPlayerSystem.currentPlayerSetupDesk;
            shopItemSystem.shopItems[2].upgradeFSM.Upgrades = currentPlayerSystem.currentPlayerSetupChair;
            shopItemSystem.shopItems[3].upgradeFSM.Upgrades = currentPlayerSystem.currentPlayerFitnessNutrition;
            shopItemSystem.shopItems[4].upgradeFSM.Upgrades = currentPlayerSystem.currentPlayerFitnessWorkout;
            shopItemSystem.shopItems[5].upgradeFSM.Upgrades = currentPlayerSystem.currentPlayerFitnessResting;
            shopItemSystem.shopItems[6].upgradeFSM.Upgrades = currentPlayerSystem.currentPlayerTrainingCoaching;
            shopItemSystem.shopItems[7].upgradeFSM.Upgrades = currentPlayerSystem.currentPlayerTrainingScrim;
            shopItemSystem.shopItems[8].upgradeFSM.Upgrades = currentPlayerSystem.currentPlayerTrainingBootcamp;
            Debug.Log("UgpradeFsm updated");
        }
    }
}
