﻿using UnityEngine;
using System.Collections;
using TMPro;

namespace PTWO_PR
{
    public class PlayerSystem : MonoBehaviour
{
    public PlayerFSM playerFSM;

    private CurrentPlayerSystem currentPlayerSystem;
    private LoadSystem loadSystem;
    private PlayerSetup playerSetup;

    public TextMeshProUGUI playerSelected;

    private void Awake()
    {
        playerFSM = GameObject.FindWithTag("PlayerFSM").GetComponent<PlayerFSM>();

        currentPlayerSystem = GameObject.FindWithTag("CurrentPlayerSystem").GetComponent<CurrentPlayerSystem>();
        loadSystem = GameObject.FindWithTag("LoadSystem").GetComponent<LoadSystem>();
        playerSetup = GameObject.FindWithTag("PlayerSetup").GetComponent<PlayerSetup>();
    }


        //loads saved data for each player
    #region ClickPlayer
    public void ClickPlayerOne()
    {
        playerFSM.PlayerSelected = 0;
        currentPlayerSystem.currentPlayerWinRate = playerSetup.playerWinRates[0];
        currentPlayerSystem.currentPlayerSetupComputer = playerSetup.playerSetupComputer[0];
        currentPlayerSystem.currentPlayerSetupDesk = playerSetup.playerSetupDesk[0];
        currentPlayerSystem.currentPlayerSetupChair = playerSetup.playerSetupChair[0];
        currentPlayerSystem.currentPlayerFitnessNutrition = playerSetup.playerFitnessNutrition[0];
        currentPlayerSystem.currentPlayerFitnessWorkout = playerSetup.playerFitnessWorkout[0];
        currentPlayerSystem.currentPlayerFitnessResting = playerSetup.playerFitnessResting[0];
        currentPlayerSystem.currentPlayerTrainingCoaching = playerSetup.playerTrainingCoaching[0];
        currentPlayerSystem.currentPlayerTrainingScrim = playerSetup.playerTrainingScrim[0];
        currentPlayerSystem.currentPlayerTrainingBootcamp = playerSetup.playerTrainingBootcamp[0];
        loadSystem.LoadUpgradeFSM();
        loadSystem.LoadPrices();
        playerSelected.text = "Player1 selected";

    }

    public void ClickPlayerTwo()
    {
        playerFSM.PlayerSelected = 1;
        currentPlayerSystem.currentPlayerWinRate = playerSetup.playerWinRates[1];
        currentPlayerSystem.currentPlayerSetupComputer = playerSetup.playerSetupComputer[1];
        currentPlayerSystem.currentPlayerSetupDesk = playerSetup.playerSetupDesk[1];
        currentPlayerSystem.currentPlayerSetupChair = playerSetup.playerSetupChair[1];
        currentPlayerSystem.currentPlayerFitnessNutrition = playerSetup.playerFitnessNutrition[1];
        currentPlayerSystem.currentPlayerFitnessWorkout = playerSetup.playerFitnessWorkout[1];
        currentPlayerSystem.currentPlayerFitnessResting = playerSetup.playerFitnessResting[1];
        currentPlayerSystem.currentPlayerTrainingCoaching = playerSetup.playerTrainingCoaching[1];
        currentPlayerSystem.currentPlayerTrainingScrim = playerSetup.playerTrainingScrim[1];
        currentPlayerSystem.currentPlayerTrainingBootcamp = playerSetup.playerTrainingBootcamp[1];
        loadSystem.LoadUpgradeFSM();
        loadSystem.LoadPrices();
        playerSelected.text = "Player2 selected";
    }

    public void ClickPlayerThree()
    {
        playerFSM.PlayerSelected = 2;
        currentPlayerSystem.currentPlayerWinRate = playerSetup.playerWinRates[2];
        currentPlayerSystem.currentPlayerSetupComputer = playerSetup.playerSetupComputer[2];
        currentPlayerSystem.currentPlayerSetupDesk = playerSetup.playerSetupDesk[2];
        currentPlayerSystem.currentPlayerSetupChair = playerSetup.playerSetupChair[2];
        currentPlayerSystem.currentPlayerFitnessNutrition = playerSetup.playerFitnessNutrition[2];
        currentPlayerSystem.currentPlayerFitnessWorkout = playerSetup.playerFitnessWorkout[2];
        currentPlayerSystem.currentPlayerFitnessResting = playerSetup.playerFitnessResting[2];
        currentPlayerSystem.currentPlayerTrainingCoaching = playerSetup.playerTrainingCoaching[2];
        currentPlayerSystem.currentPlayerTrainingScrim = playerSetup.playerTrainingScrim[2];
        currentPlayerSystem.currentPlayerTrainingBootcamp = playerSetup.playerTrainingBootcamp[2];
        loadSystem.LoadUpgradeFSM();
        loadSystem.LoadPrices();
        playerSelected.text = "Player3 selected";
    }

    public void ClickPlayerFour()
    {
        playerFSM.PlayerSelected = 3;
        currentPlayerSystem.currentPlayerWinRate = playerSetup.playerWinRates[3];
        currentPlayerSystem.currentPlayerSetupComputer = playerSetup.playerSetupComputer[3];
        currentPlayerSystem.currentPlayerSetupDesk = playerSetup.playerSetupDesk[3];
        currentPlayerSystem.currentPlayerSetupChair = playerSetup.playerSetupChair[3];
        currentPlayerSystem.currentPlayerFitnessNutrition = playerSetup.playerFitnessNutrition[3];
        currentPlayerSystem.currentPlayerFitnessWorkout = playerSetup.playerFitnessWorkout[3];
        currentPlayerSystem.currentPlayerFitnessResting = playerSetup.playerFitnessResting[3];
        currentPlayerSystem.currentPlayerTrainingCoaching = playerSetup.playerTrainingCoaching[3];
        currentPlayerSystem.currentPlayerTrainingScrim = playerSetup.playerTrainingScrim[3];
        currentPlayerSystem.currentPlayerTrainingBootcamp = playerSetup.playerTrainingBootcamp[3];
        loadSystem.LoadUpgradeFSM();
        loadSystem.LoadPrices();
        playerSelected.text = "Player4 selected";
    }
    #endregion
}
}

