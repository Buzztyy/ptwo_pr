﻿using UnityEngine;
using System.Collections;
using FSM;
using System;
using DG.Tweening;

namespace PTWO_PR
{
    public enum TournamentConclusionFsmState
    {
        IDLE,
        FIRSTROUNDWON,
        FIRSTROUNDLOST,
        SECONDROUNDWON,
        SECONDROUNDLOST,
        THIRDROUNDWON,
        THIRDROUNDLOST
    }

    public class TournamentConclusionFSM : MonoBehaviour
    {
        [SerializeField] private TournamentConclusionFsmState[] states;
        [SerializeField] private TournamentConclusionFsmState startState;
        [SerializeField] private int tournamentStage;
        [SerializeField] private TournamentConclusionSystem tournamentConclusionSystem;

        public int TournamentStage
        {
            get { return tournamentStage; }
            set { tournamentStage = value; }
        }

        [SerializeField] private TournamentConclusionFsmState currentState;

        private StateMachine fsm;

        private void Awake()
        {
            tournamentConclusionSystem = GameObject.FindWithTag("TournamentConclusionSystem").GetComponent<TournamentConclusionSystem>();
        }


        private void Start()
        {

            fsm = new StateMachine();

            for (int i = 0; i < states.Length; i++)
            {

                fsm.AddState(states[i].ToString(), onEnter: OnEnterState, onLogic: OnLogicState);
            }

            fsm.AddTransition(TournamentConclusionFsmState.IDLE.ToString(), TournamentConclusionFsmState.FIRSTROUNDWON.ToString(),
               FromIdleToFirstRoundWon);

            fsm.AddTransition(TournamentConclusionFsmState.IDLE.ToString(), TournamentConclusionFsmState.FIRSTROUNDLOST.ToString(),
                FromIdleToFirstRoundLost);

            fsm.AddTransition(TournamentConclusionFsmState.FIRSTROUNDWON.ToString(), TournamentConclusionFsmState.SECONDROUNDWON.ToString(),
                FromFirstRoundWonToSecondRoundWon);

            fsm.AddTransition(TournamentConclusionFsmState.FIRSTROUNDWON.ToString(), TournamentConclusionFsmState.SECONDROUNDLOST.ToString(),
                FromFirstRoundWonToSecondRoundLost);

            fsm.AddTransition(TournamentConclusionFsmState.SECONDROUNDWON.ToString(), TournamentConclusionFsmState.THIRDROUNDWON.ToString(),
                FromSecondRoundWonToThirdRoundWon);

            fsm.AddTransition(TournamentConclusionFsmState.SECONDROUNDWON.ToString(), TournamentConclusionFsmState.THIRDROUNDLOST.ToString(),
                FromSecondRoundWonToThirdRoundLost);


            fsm.SetStartState(startState.ToString());

            fsm.Init();
        }


        private void Update()
        {
            fsm.OnLogic();
        }

        private void OnEnterState(State<string> obj)
        {

            currentState = (TournamentConclusionFsmState)Enum.Parse(typeof(TournamentConclusionFsmState), obj.fsm.ActiveStateName);

            if (obj.fsm.ActiveStateName == TournamentConclusionFsmState.IDLE.ToString())
            {
                Debug.Log("Entering IDLE State");
            }

            if (obj.fsm.ActiveStateName == TournamentConclusionFsmState.FIRSTROUNDWON.ToString())
            {
                Debug.Log("Entering FIRSTROUNDWON State");
            }

            if (obj.fsm.ActiveStateName == TournamentConclusionFsmState.FIRSTROUNDLOST.ToString())
            {
                Debug.Log("Entering FIRSTROUNDLOST State");
            }

            if (obj.fsm.ActiveStateName == TournamentConclusionFsmState.SECONDROUNDWON.ToString())
            {
                Debug.Log("Entering SECONDROUNDWON State");
            }

            if (obj.fsm.ActiveStateName == TournamentConclusionFsmState.SECONDROUNDLOST.ToString())
            {
                Debug.Log("Entering SECONDROUNDLOST State");
            }

            if (obj.fsm.ActiveStateName == TournamentConclusionFsmState.THIRDROUNDWON.ToString())
            {
                Debug.Log("Entering THIRDROUNDWON State");
            }

            if (obj.fsm.ActiveStateName == TournamentConclusionFsmState.THIRDROUNDLOST.ToString())
            {
                Debug.Log("Entering THIRDROUNDLOST State");
            }
        }

        //starts different coroutines inside the "TournamentConclusionSystem" script based on the tournament stage
        #region Transitionchanges
        private bool FromIdleToFirstRoundWon(Transition<string> arg)
        {
            if (tournamentStage == 1)
            {
                StartCoroutine(tournamentConclusionSystem.FirstRoundWon());
                Debug.Log("Transition successfull");
            }

            return tournamentStage == 1;
        }

        private bool FromIdleToFirstRoundLost(Transition<string> arg)
        {
            if (tournamentStage == 2)
            {
                StartCoroutine(tournamentConclusionSystem.FirstRoundLost());
                Debug.Log("Transition sucessfull");
            }

            return tournamentStage == 2;
        }

        private bool FromFirstRoundWonToSecondRoundWon(Transition<string> arg)
        {
            if (tournamentStage == 3)
            {
                StartCoroutine(tournamentConclusionSystem.SecondRoundWon());
                Debug.Log("Transition sucessfull");
            }

            return tournamentStage == 3;
        }

        private bool FromFirstRoundWonToSecondRoundLost(Transition<string> arg)
        {
            if (tournamentStage == 4)
            {
                StartCoroutine(tournamentConclusionSystem.SecondRoundLost());
                Debug.Log("Transition sucessfull");
            }

            return tournamentStage == 4;
        }

        private bool FromSecondRoundWonToThirdRoundWon(Transition<string> arg)
        {
            if (tournamentStage == 5)
            {
                StartCoroutine(tournamentConclusionSystem.ThirdRoundWon());
                Debug.Log("Transition sucessfull");
            }

            return tournamentStage == 5;
        }

        private bool FromSecondRoundWonToThirdRoundLost(Transition<string> arg)
        {
            if (tournamentStage == 6)
            {
                StartCoroutine(tournamentConclusionSystem.ThirdRoundLost());
                Debug.Log("Transition sucessfull");
            }

            return tournamentStage == 6;
        }

        private bool FromRoundToIdle(Transition<string> arg)
        {
            if (tournamentStage <= 0)
            {
                Debug.Log("Transition sucessfull");
            }

            return tournamentStage <= 0;
        }
        #endregion

        private void OnLogicState(State<string> obj)
        {
            if (obj.fsm.ActiveStateName == TournamentConclusionFsmState.IDLE.ToString())
            {
                OnIdleUpdate();
            }

            if (obj.fsm.ActiveStateName == TournamentConclusionFsmState.FIRSTROUNDWON.ToString())
            {
                OnFirstRoundWonUpdate();
            }

            if (obj.fsm.ActiveStateName == TournamentConclusionFsmState.FIRSTROUNDLOST.ToString())
            {
                OnFirstRoundLostUpdate();
            }

            if (obj.fsm.ActiveStateName == TournamentConclusionFsmState.SECONDROUNDWON.ToString())
            {
                OnSecondRoundWonUpdate();
            }

            if (obj.fsm.ActiveStateName == TournamentConclusionFsmState.SECONDROUNDLOST.ToString())
            {
                OnSecondRoundLostUpdate();
            }

            if (obj.fsm.ActiveStateName == TournamentConclusionFsmState.THIRDROUNDWON.ToString())
            {
                OnThirdRoundWonUpdate();
            }

            if (obj.fsm.ActiveStateName == TournamentConclusionFsmState.THIRDROUNDLOST.ToString())
            {
                OnThirdRoundLostUpdate();
            }
        }

        private void OnIdleUpdate()
        {
            Debug.Log("In Idle");
        }

        private void OnFirstRoundWonUpdate()
        {
            Debug.Log("In First Round Won");
        }

        private void OnFirstRoundLostUpdate()
        {
            Debug.Log("In First Round Lost");
        }

        private void OnSecondRoundWonUpdate()
        {
            Debug.Log("In Second Round Won");
        }

        private void OnSecondRoundLostUpdate()
        {
            Debug.Log("In Second Round Lost");
        }

        private void OnThirdRoundWonUpdate()
        {
            Debug.Log("In Third Round Won");
        }

        private void OnThirdRoundLostUpdate()
        {
            Debug.Log("In Third Round Lost");
        }
    }
}
