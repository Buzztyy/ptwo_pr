using FSM;
using UnityEngine;

namespace PGaS.IntermediateScripting._09_FiniteStateMachine
{
    /// <summary>
    /// This FSM Behaviour utilizes the FSM System UnityHFSM from github
    /// for description pls visit: https://github.com/Inspiaaa/UnityHFSM
    /// This is lightweight example for you to create an FSM from scratch with this system
    /// It demonstrates the minimum code you need to write to:
    /// - create a fsm
    /// - init a fsm
    /// - add states to a fsm
    /// - add transition to a fsm
    /// - run a fsm
    public class SimpleFsm : MonoBehaviour
    {
        /// <summary>
        /// An int value we use to transition from a state to another state
        /// for demonstration purposes
        /// </summary>
        [SerializeField]
        private int speed;

        /// <summary>
        /// The fsm object we need to actually work with
        /// </summary>
        private StateMachine fsm;

        private void Start()
        {
            // In order to work with the statemachine, we need to create the object instance
            fsm = new StateMachine();

            // After creating the instance, we can perform the necessary operations on the FSM object
            // with the respective methods
            // with .AddState we can add a state we want to use within the statemachine 
            // .AddState absolutely needs a state represented as a string, this is the minimum requirement
            // After that the other parameters are optional
            // You can feed these paramters as well, which is optional:
            // - an onEnter method, this method will be called once when the state you passed in will be entered
            // - an onLogic method, this method will be called every frame when the state you passed in is active
            // - an onExit method, this method will be called once when the state you passed in will be exited
            fsm.AddState("IDLE", OnEnterIdle, OnLogicIdle, OnExitIdle);
            fsm.AddState("WALK", OnEnterWalk, OnLogicWalk);

            // The idea of StateMachines is to go from one state to the other under specific conditions
            // With this StateMachine System you can call the .AddTransition method to do this
            // You need to specify from which state you want to go into the next state
            // As further parameter, you can specify the condition as a method with a bool return type
            fsm.AddTransition("IDLE", "WALK", FromIdleToWalk);

            // This method call is optional, but depending on which state you want to be the first state
            // you probably should call this method nevertheless
            // If you don't call this method the fsm object will make the very first state you added to the fsm
            // as the start state
            fsm.SetStartState("IDLE");

            // At the end we must call this method to init the fsm before the Update logic
            fsm.Init();
        }

        private void Update()
        {
            // In Update we want our fsm to "flow", so we need to call the .OnLogic Method
            // for this to work
            fsm.OnLogic();
        }


        /// <summary>
        /// This Method will be called when the Idle State will be entered
        /// </summary>
        /// <param name="obj"></param>
        private void OnEnterIdle(State<string> obj)
        {
            Debug.Log("Idle state entered");
        }

        /// <summary>
        /// This Method will be called every frame as long as we are in the Idle state
        /// </summary>
        /// <param name="obj"></param>
        private void OnLogicIdle(State<string> obj)
        {
            Debug.Log("Idle is idling");
        }

        /// <summary>
        /// This Method will be called once when are exiting the Idle State
        /// </summary>
        /// <param name="obj"></param>
        private void OnExitIdle(State<string> obj)
        {
            Debug.Log("Idle was exit");
        }

        /// <summary>
        /// This Method is called by our fsm every frame to check if the condition
        /// is true
        /// This happens to make sure that we can transition from the Idle State
        /// to the Walk State
        /// In this example we transition from Idle to Walk when the speed value is higher or equal to 5
        /// If that's true, we change the state from Idle to Walk
        /// If that's false, we stay in the Idle state
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private bool FromIdleToWalk(Transition<string> arg)
        {
            if (speed >= 5)
            {
                Debug.Log("Transition successfull");
            }

            return speed >= 5;
        }

        /// <summary>
        /// This method will be called once after the Transition from Idle to Walk
        /// Meaning after the FromIdleToWalk method returns true
        /// Our fsm object will update itself and enter the new state, in this case our Walk State
        /// </summary>
        /// <param name="obj"></param>
        private void OnEnterWalk(State<string> obj)
        {
            Debug.Log("Walk was entered");
        }

        /// <summary>
        /// This Method will be called every frame as long as we are in the Walk state
        /// </summary>
        /// <param name="obj"></param>
        private void OnLogicWalk(State<string> obj)
        {
            Debug.Log("Walk is walking");
        }
    }
}