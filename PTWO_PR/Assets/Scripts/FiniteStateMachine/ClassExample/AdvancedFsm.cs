using System;
using FSM;
using UnityEngine;

namespace PGaS.IntermediateScripting._09_FiniteStateMachine
{
    /// <summary>
    /// A simple enum we create to handle States in our FSM
    /// </summary>
    public enum SimpleFsmState
    {
        IDLE,
        WALK
    }

    /// <summary>
    /// This FSM Behaviour utilizes the FSM System UnityHFSM from github
    /// for description pls visit: https://github.com/Inspiaaa/UnityHFSM
    /// This is lightweight example for you to create an FSM from scratch with this system
    /// It demonstrates the minimum code you need to write to:
    /// - create a fsm
    /// - init a fsm
    /// - add states to a fsm
    /// - add transition to a fsm
    /// - run a fsm   
    /// Furthermore this scripts shows how you CAN utilize enums to make at least some parts of the logic
    /// visible to the Unity Inspector
    /// </summary>
    public class AdvancedFsm : MonoBehaviour
    {
        /// <summary>
        /// An array of our states
        /// So we can add these states in a simple for loop instead of adding
        /// all potential states manually in code
        /// </summary>
        [SerializeField]
        private SimpleFsmState[] states;

        /// <summary>
        /// A state we can set from the Unity Inspector as the start state,
        /// so we can do it there instead of hardcoding it
        /// </summary>
        [SerializeField]
        private SimpleFsmState startState;

        /// <summary>
        /// An int value we use to transition from a state to another state
        /// for demonstration purposes
        /// </summary>
        [SerializeField]
        private int speed;

        /// <summary>
        /// A conveniance state so we can see in which state we are currently in
        /// </summary>
        [SerializeField]
        private SimpleFsmState currentState;


        /// <summary>
        /// The fsm object we need to actually work with
        /// </summary>
        private StateMachine fsm;


        private void Start()
        {
            // In order to work with the statemachine, we need to create the object instance
            fsm = new StateMachine();

            for (int i = 0; i < states.Length; i++)
            {
                // After creating the instance, we can perform the necessary operations on the FSM object
                // with the respective methods
                // with .AddState we can add a state we want to use within the statemachine 
                // .AddState absolutely needs a state represented as a string, this is the minimum requirement
                // After that the other parameters are optional
                // You can feed these paramters as well, which is optional:
                // - an onEnter method, this method will be called once when the state you passed in will be entered
                // - an onLogic method, this method will be called every frame when the state you passed in is active
                // - an onExit method, this method will be called once when the state you passed in will be exited

                // Since we have an array of states we need to be bit more generic
                // Since we need to pass in a string, we can utilize the .ToString method of our enum state
                fsm.AddState(states[i].ToString(), onEnter: OnEnterState, onLogic: OnLogicState);
            }

            // The idea of StateMachines is to go from one state to the other under specific conditions
            // With this StateMachine System you can call the .AddTransition method to do this
            // You need to specify from which state you want to go into the next state
            // As further parameter, you can specify the condition as a method with a bool return type

            // Instead of hard strings we can utilize again the .ToString method of our state enum values
            // which is way more readable and less error prone then a hard string
            fsm.AddTransition(SimpleFsmState.IDLE.ToString(), SimpleFsmState.WALK.ToString(), FromIdleToWalk);

            // This method call is optional, but depending on which state you want to be the first state
            // you probably should call this method nevertheless
            // If you don't call this method the fsm object will make the very first state you added to the fsm
            // as the start state

            // Here again: Instead of a hardcoded string we can utilize our variable we setup above and
            // utilize the .ToString method to get the desired string
            fsm.SetStartState(startState.ToString());

            // At the end we must call this method to init the fsm before the Update logic
            fsm.Init();
        }


        private void Update()
        {
            // In Update we want our fsm to "flow", so we need to call the .OnLogic Method
            // for this to work
            fsm.OnLogic();
        }

        /// <summary>
        /// This method will be called once a state has been entered
        /// Since we use an array we don't know exactly which state we entered right now from
        /// the array index
        /// BUT! We get the current active state as an parameter in the State<string> obj parameter
        /// This way we can compare it and act accordingly
        /// </summary>
        /// <param name="obj"></param>
        private void OnEnterState(State<string> obj)
        {
            // In order to see which state we entered we parse the the obj.fsm.ActiveStateName
            // to our currentState enum variable
            currentState = (SimpleFsmState)Enum.Parse(typeof(SimpleFsmState), obj.fsm.ActiveStateName);
            
            // We compare which state is the active state which was currently entered
            // Depending on the state we perform different things
            if (obj.fsm.ActiveStateName == SimpleFsmState.IDLE.ToString())
            {
                Debug.Log("Entering Idle State");
            }

            if (obj.fsm.ActiveStateName == SimpleFsmState.WALK.ToString())
            {
                Debug.Log("Entering Walk State");
            }
        }

        /// <summary>
        /// This Method is called by our fsm every frame to check if the condition
        /// is true
        /// This happens to make sure that we can transition from the Idle State
        /// to the Walk State
        /// In this example we transition from Idle to Walk when the speed value is higher or equal to 5
        /// If that's true, we change the state from Idle to Walk
        /// If that's false, we stay in the Idle state
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private bool FromIdleToWalk(Transition<string> arg)
        {
            if (speed >= 5)
            {
                Debug.Log("Transition successfull");
            }

            return speed >= 5;
        }

        /// <summary>
        /// This method will be called every frame we are in a specific state
        /// Since we use an array we don't know exactly which state we entered and are currently in right now from
        /// the array index
        /// BUT! We get the current active state as an parameter in the State<string> obj parameter
        /// This way we can compare it and act accordingly
        /// </summary>
        /// <param name="obj"></param>
        private void OnLogicState(State<string> obj)
        {
            // We compare which state is the active state which was currently entered
            // Depending on the state we perform different things
            // In order to tidy things up, we just call new methods were we can perform
            // various tasks
            if (obj.fsm.ActiveStateName == SimpleFsmState.IDLE.ToString())
            {
                OnIdleStateUpdate();
            }

            if (obj.fsm.ActiveStateName == SimpleFsmState.WALK.ToString())
            {
                OnWalkStateUpdate();
            }
        }

        /// <summary>
        /// This method will be called every frame once we are in the idle state
        /// </summary>
        private void OnIdleStateUpdate()
        {
            Debug.Log("Bin in Idle");
        }

        /// <summary>
        /// This method will be called every frame once we are in the walk state
        /// </summary>
        private void OnWalkStateUpdate()
        {
            Debug.Log("Bin in Walk");
        }
    }
}