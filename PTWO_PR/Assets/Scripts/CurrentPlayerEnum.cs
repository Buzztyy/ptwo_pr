﻿using UnityEngine;
using System.Collections;


namespace PTWO_PR
{
    // This Script contains an Enum to store the Player-States
    public enum CurrentPlayerEnum
    {
        PLAYERONE,
        PLAYERTWO,
        PLAYERTHREE,
        PLAYERFOUR
    }
}

