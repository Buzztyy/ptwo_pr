﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace PTWO_PR
{
    public class WinrateBar : MonoBehaviour
    {
        //This script manages the winrate Slider
        public Image winRateSlider;

        public void SetWinRate(float winRate)
        {
            winRateSlider.fillAmount = winRate;
        }
    }
}
