﻿using UnityEngine;
using System.Collections;
using TMPro;

namespace PTWO_PR
{
    public class MoneySystem : MonoBehaviour
    {
        private DataManager dataManager;

        public TextMeshProUGUI computerPrice;
        public TextMeshProUGUI deskPrice;
        public TextMeshProUGUI chairPrice;
        public TextMeshProUGUI nutritionPrice;
        public TextMeshProUGUI workoutPrice;
        public TextMeshProUGUI restingPrice;
        public TextMeshProUGUI coachingPrice;
        public TextMeshProUGUI scrimPrice;
        public TextMeshProUGUI bootcampPrice;

        public TextMeshProUGUI money;
        public TextMeshProUGUI noMoney;

        private void Awake()
        {
            dataManager = GameObject.FindWithTag("DataManager").GetComponent<DataManager>();
        }

        //updates the currency in the UI
        public void MoneyUpdate()
        {
            money.text = "Money: " + dataManager.data.money.ToString() + " $";
        }

        //feedback for unsufficient money
        public void NoMoneyText()
        {
            StartCoroutine(NoMoneyRoutine());
        }

        IEnumerator NoMoneyRoutine()
        {
            noMoney.text = "NO MONEY";
            yield return new WaitForSeconds(0.5f);
            noMoney.text = "";
        }
    }
}

