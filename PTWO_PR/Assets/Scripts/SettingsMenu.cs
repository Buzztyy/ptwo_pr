using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace PTWO_PR
{
    public class SettingsMenu : MonoBehaviour
    {
        [SerializeField] private GameObject mainMenu;
        [SerializeField] private GameObject settingsMenu;
        [SerializeField] private GameObject creditsMenu;
        
        
        public void BackToMenu()
        { 
            creditsMenu.SetActive(false);
            settingsMenu.SetActive(false);
            mainMenu.SetActive(true);
        }

        public void Credits()
        { 
            creditsMenu.SetActive(true);
            mainMenu.SetActive(false);
        }
        
        public void Settings()
        { 
            settingsMenu.SetActive(true);
            mainMenu.SetActive(false);
        }
        public AudioMixer audioMixer;

        //method to set volume for audio mixer
        public void SetVolume(float volume)
        {
            audioMixer.SetFloat("Volume", volume);
        }

        //method to set unity quality settings to specific quality levels
        public void SetQuality(int qualityIndex)
        {
            QualitySettings.SetQualityLevel(qualityIndex);
        }

        //method to activate/deactivate fullscreen
        public void SetFullscreen(bool isFullscreen)
        {
            Screen.fullScreen = isFullscreen;
        }
        
        
    }
}
